<?php
	// 'nome_da_rota'=> 'legenda'
	return [
		'profile' => [
			'label'=>'Perfil de usuário',
		],

		'user' => [
			'label'=>'Usuários',
		],

		'area' => [
			'label'=>'Área',
		],

		'menu' => [
			'label'=>'Menus',
		],

		'submenu' => [
			'label'=>'Submenus',
		],

		'mail' => [
			'label' => 'Enviar e-mail',
		],

	];