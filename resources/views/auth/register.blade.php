@extends('layouts.app')

@section('title')
    Usuários do sistema
@stop

@section('description')
    Cadastro dos usuários do sistema
@stop

@section('action')
    Dados do usuário
@stop

@section('content')
    {{-- Observa se a requisicao passa um model como parametro --}}
    @php
        // Caso não haja chama o metodo store para criar um registro
        $action = [
            'route' => 'register',
            'enctype' => "multipart/form-data",
        ];
        // Caso haja, muda a rota e chama o metodo update passando como parametro o id do registro a ser editado
        // if (isset($model))
        //     $action = [
        //         // rota a ser chamada
        //         'route' => ['user.update', $model->id],
        //         // invoca o metodo PUT
        //         'method' => 'PUT',
        //         'enctype' => "multipart/form-data",
        //     ];

    @endphp

    {{ Form::open($action) }}
        {{-- caso exista o model, prepara o formulario com os dados do registro passado pelo id --}}
        @if (isset($old))
            @php
                Form::setModel($model);
            @endphp
        @endif
        {{ csrf_field() }}
    <div class="row">
        {{-- Lado esquerdo foto --}}
        <div class="col-md-4 well">
            <div class="col-sm-8">
                <a href="#" class="thumbnail">
                  <img src="{{ asset('img/avatar5.png') }}" alt="Foto do usuário" name="img" alt="Foto do usuário" class="img-40-40" />
                </a>
                {{ Form::file('imagem', null, ['class'=>'form-control control-file-input', 'id'=>'file']) }}
            </div>
        </div>
    {{-- Lado direito dados do usuário --}}
        <div class="col-md-8">

            <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fas fa-user"></i></span>
                    <input type="text", name="name", class="form-control", title="Informe o nome do usuário", value="{{ old('name') }}">
                </div>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="control-group{{ $errors->has('cpf') ? ' has-error' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon">12</span>
                    <input type="text" name="cpf" id="cpf" class="form-control" title="Informe o número do cpf..." value="{{ old('cpf') }}">
                </div>

                @if ($errors->has('cpf'))
                    <span class="help-block">
                        <strong>{{ $errors->first('cpf') }}</strong>
                    </span>
                @endif
            </div>

            <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="far fa-envelope"></i></span>
                    {{ Form::text('email', null, ['class'=>'form-control', 'id'=>'email', 'title'=>'Informe o e-mail do usuário']) }}
                </div>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            @inject('funcao', 'App\Funcao')
            @php
                $funcaoOptions = $funcao::pluck('descricao', 'id');
            @endphp

            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-briefcase"></i></span>
                    {{ Form::select('funcao_id', $funcaoOptions,  null,['class'=>'form-control','title'=>'Selecione a função para o usuários']) }}
                </div>
            </div>

            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="far fa-address-card"></i></span>
                    {{ Form::text('num_conselho', null, ['class'=>'form-control', 'id'=>'email', 'title'=>'Informe o número do conselho de classe']) }}
                </div>
            </div>

            @inject('encaminharSetor', 'App\EncaminharSetor')
            @php
                $encaminharOptions = $encaminharSetor::pluck('descricao', 'id');
            @endphp
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="far fa-hand-point-right"></i></span>
                    {{ Form::select('encaminhar_setor_id', $encaminharOptions,  null,['class'=>'form-control','title'=>'Selecione o setor de trabalho']) }}
                </div>
            </div>

            @inject('profile', 'App\Profile')
            @php
                $profileOptions = $profile::pluck('name', 'id');
            @endphp

            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-check"></i></span>
                    {{ Form::select('profile_id', $profileOptions, null ,['class'=>'form-control', 'multiple'=>true, 'title'=>'Selecione um mais perfil para o usuários']) }}
                </div>
            </div>

            <div class="control-group{{ $errors->first('password') ? ' has-error' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" name="password" id="password" class="form-control" title="Informe a senha">
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-retweet"></i></span>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" title="Confirme a senha" required>
                </div>
            </div>

            <div class="control-group">
                <a href="{{ route('user.index') }}" class="btn btn-default">
                    <i class="fa fa-arrow-left"></i>
                    Voltar
                </a>
                {{ Form::submit('enviar', ['class'=>'btn btn-success btn-save']) }}
            </div>
        </div>
    </div>

    {{ Form::close() }}
@stop

@section('footer')
    {{-- <div class="alert alert-warning">Atenção</div> --}}
@stop