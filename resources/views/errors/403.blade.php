@extends('layouts.lte-danger')

@section('title')
    Página de erro
@stop

@section('description')
    Informação de erro reportada pelo sistema
@stop

@section('action')
    404 - ACESSO NEGADO!
@stop

@section('content')

@stop

@section('footer')
    <div class="text-center">
        <h1 class="text-red">
            <i class="fa fa-warning fa-1x"></i>
            <strong>Acesso negado!</strong>
        </h1>
        <h4>
            <p class="text-black">
                Verificamos em nosso sistema que seu perfil de usuário não possui permissão para acessar esta área do sistema.
            </p>
        </h4>
    </div>
@stop