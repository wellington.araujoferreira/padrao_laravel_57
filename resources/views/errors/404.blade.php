@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row center">
        <div class="col-md-6 col-md-offset-2">
            <div class="panel panel-danger blocktext">
                <div class="panel-heading">
                    <h3>
                        <strong class="text-color-red">
                            Atenção
                            <i class="fa fa-exclamation"></i>
                        </strong>
                    </h3>
                </div>
                <div class="panel-body">
                    <div>
                        <h1><i class="fa fa-file-text-o fa-5x blocktext text-color-red"></i></h1>
                    </div>
                    <div class="text-color-red"><h3>Local inexistente!</h3></div>
                </div>
                <div class="panel-footer">O local solicitado não existe.</div>
            </div>
        </div>
    </div>
</div>
@endsection