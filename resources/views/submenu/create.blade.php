@extends('layouts.app')

@section('title')
	Submenu
@stop

@section('description')
	Cadastro de Submenus do sistema
@stop

@section('action')
	Dados do Submenu
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			'route' => 'submenu.store',
			'class' => 'form-horizontal'
		];
		// Caso haja, muda a rota e chama o metodo update passando como parametro o id do registro a ser editado
		if (isset($model))
			$action = [
				// rota a ser chamada
				'route' => ['submenu.update', $model->id],
				// invoca o metodo PUT
				'method' => 'PUT'
			];

	@endphp

	{{ Form::open($action)}}
		{!! csrf_field() !!}
		{{-- caso exista o model, prepara o formulario com os dados do registro passado pelo id --}}
		@if (isset($model))
			@php
				Form::setModel($model);
			@endphp
		@endif

	@inject('menu', 'App\Menu')
	@php
		$options = $menu::pluck('name', 'id')->toArray();
	@endphp

	<div class="box-body">

		<div class="form-group">
			{{ Form::label('menu_id', 'Menu') }}
			{{ Form::select('menu_id', $options, null, ['class'=>'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('name', 'Submenu') }}
			{{ Form::text('name', null, ['class'=>'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('description', 'Descrição') }}
			{{ Form::text('description', null, ['class'=>'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('action', 'Ação') }}
			{{ Form::text('action', null, ['class'=>'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('route', 'Rota') }}
			{{ Form::text('route', null, ['class'=>'form-control']) }}
		</div>

		<div class="form-group">
			{{ Form::label('icon', 'Icone') }}
			{{ Form::text('icon', null, ['class'=>'form-control']) }}
		</div>

		<div class="form-group">
			<a href="{{ route('submenu.index') }}" class="btn btn-default">
				<i class="fa fa-arrow-left"></i>
				Voltar
			</a>
			{{ Form::submit('enviar', ['class'=>'btn btn-success btn-save']) }}
		</div>


	</div>

	{{ Form::close() }}
@stop

@section('footer')
	{{--  --}}
@stop