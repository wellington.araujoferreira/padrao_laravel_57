@extends('layouts.app')

@section('title')
	Submenu 
@stop

@section('description')
	Cadastro de Submenus do sistema
@stop

@section('action')
	Listagem dos Submenus cadastrados
@stop

@if (Auth::user()->hasRules('submenu.store'))
	@section('plus')
		<a href="{{route('submenu.create') }}" class="btn btn-info" data-toggle="tooltip" title="Novo Submenu">
			<i class="fa fa-bolt"></i>
		</a>
	@stop
@endif

@section('content')

<div class="well">
	@include('submenu.search')
</div>

<div class="table table-responsive table-condensed table-hover">
	<table class='table'>
		<tr>
			<th>#</th>
			<th>Menu</th>
			<th>Submenu</th>
			<th>Descrição</th>
			<th>Ação</th>
			<th>Rota</th>
			<th>Icone</th>
			<th><i class="fa fa-arrow-down"></i></th>
			<th></th>
		</tr>

		@foreach ($models as $m)
			<tr>
				<td>{{ $m->id }}</td>
				<td>
					{{ $m->menu->name }}
				</td>
				<td>{{ $m->name }}</td>
				<td>{{ $m->description }}</td>
				<td>{{ $m->action }}</td>
				<td>{{ $m->route }}</td>
				<td>{{ $m->icon }}</td>
				<td> <i class="fa {{ $m->icon }}"></i></td>
				<td width="12%">
					@include('includes.helper.action', ['model'=>$m, 'route'=>'submenu'])
				</td>
			</tr>
		@endforeach
	</table>
</div>
@stop