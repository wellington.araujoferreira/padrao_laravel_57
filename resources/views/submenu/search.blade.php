{{ Form::open([ 'route' => 'submenu.index', 'method'=>'GET', 'class'=>'form-inline']) }}
	{!! csrf_field() !!}

	<div class="box box-solid {{ config('app.box-theme') }}">
		<div class="box-header with-border">
			<h3 class="box-title">
				<i class="fa fa-search"></i>
				Buscar
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Esconder...">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Fechar..." disabled="true">
						<i class="fa fa-times"></i>
				</button>
			</div>
		</div>

		<div class="box-body form-horizontal">
			<div class="box-body">
				<div class="form-group">
					{{-- {{ Form::label('id', 'Registro', ['class'=>'control-label col-sm-2']) }} --}}
					<div class="col-sm-1">
						{{ Form::text('id', $id, ['class'=>'form-control', 'placeholder'=>'ID','title'=>'Nº de registro']) }}
					</div>
					<div class="col-sm-2">
						{{ Form::text('menu', $menu, ['class'=>'form-control', 'title'=>'Nome do menu', 'placeholder' => 'Menu', 'style'=>'width:100%']) }}
					</div>
					<div class="col-sm-3">
						{{Form::text('submenu', $submenu, ['class'=>'form-control', 'title'=>'Nome do submenu', 'placeholder'=> 'Submenu','style'=>'width:100%'])}}
					</div>
					<div class="col-sm-3">
						{{Form::text('description', $description, ['class'=>'form-control', 'title'=>'Descrição do submenu', 'placeholder'=> 'Descrição','style'=>'width:100%'])}}
					</div>
					<div class="col-sm-3">
						<div class="btn-group role="group" aria-label="Basic example">
							<a href="{{ Route('submenu.index') }}" class="form-control btn btn-default btn-sm">	<i class="fa fa-trash"></i>Limpar</a>
							{{ Form::button('<i class="fa fa-search"></i> Buscar', ['type'=>'submit', 'class'=>'form-control btn-primary btn-sm']) }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
{{ Form::close() }}