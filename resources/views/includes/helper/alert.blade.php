@if (isset($errors))
    @if (count($errors) > 0)
        @php
            $msg = '';
            foreach ($errors->all() as $error) {
                $msg .= $error . '\n';
            }

            session()->flash('warning', $msg);
        @endphp
    @endif
@endif


@section('alert')

    @if (session('success'))
        <script>
            swal("Sucesso!", "{{ session('success') }}", "success")
        </script>
    @endif

    @if (session('danger'))
        <script>
            swal("Erro!", "{{ session('danger') }}", "error")
        </script>
    @endif

    @if (session('warning'))
        <script>
            swal("Atenção!", "{{ session('warning') }}", "warning")
        </script>
    @endif

    @if (session('info'))
        <script>
            swal("Informação!", "{{ session('info') }}", "info")
        </script>
    @endif

    @if (session('listaRetorno'))
        @include('includes.helper.alert-list')
    @endif
@stop