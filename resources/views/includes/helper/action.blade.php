@if (Auth::user()->hasRules($route . '.update'))

	<a href="{{ route($route . '.edit', $m->id) }}" class="btn btn-default btn-sm" title="Editar">
		<i class="fa fa-edit"></i>
	</a>
@endif

@if (Auth::user()->hasRules($route . '.destroy'))
	<div class="btn-group">
		{!! Form::open([
			'method' => 'DELETE',
			'class' => 'pull-right',
			'route' => [$route . '.destroy', $model->id]
			]) !!}

			<button type="button" class="btn btn-danger btn-alert btn-sm">
				<i class="fa fa-trash"></i>
			</button>

		{!! Form::close() !!}
	</div>
@endif