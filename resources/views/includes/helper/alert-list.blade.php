@php
	$retorno = session('listaRetorno');
@endphp

<div class="alert alert-success">
	<label for="helper" class="label-success">Bem sucedidos:</label>
	@foreach ($retorno['success'] as $success)
		{{ $success }}
	@endforeach
</div>