<?php 
	$msg = '';
	foreach ($errors->all() as $error) {
		$msg .= $error . '\n';
	}

	session()->flash('warning', $msg);

?>