<!-- Authentication Links -->
@if (Auth::guest())
    {{-- <li><a href="{{ route('login') }}">Login</a></li> --}}
    {{-- <li><a href="{{ route('register') }}">Register</a></li> --}}
@else
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
            <i class="fa fa-user-circle-o"></i>
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="">
                    <i class="fa fa-edit"></i>
                    Editar meus dados
                </a>
            </li>

            {{-- <li>
                <a href="">
                    <i class="fa fa-user"></i>
                    Mensagens
                </a>
            </li> --}}
            {{-- <li><a href=""><i class="fa fa-user"></i></a></li> --}}
            <li>
                <a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i>
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
    </li>
@endif