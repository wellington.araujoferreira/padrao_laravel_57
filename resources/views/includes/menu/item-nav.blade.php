@if (Auth::user()->hasRules($route))
	<li>
		<a href="{{ route($route) }}">
		<i class="fa {{ $icon }} text-aqua"></i> {{ $label }}</a>
	</li>
@endif