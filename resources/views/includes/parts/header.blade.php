<header class="main-header">
    <!-- Logo -->
    <a href="{{ url('/')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><strong class="text-color-cyan">DF</strong>AP</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <img src="{{ asset('img/defenap.png' )}}" width="30" height="30">
            <strong class="text-color-cyan">{{ config('app.name') }}</strong>{{ config('app.type') }}
        </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                {{-- @include('includes.parts.header.messages') --}}

                <!-- Notifications: style can be found in dropdown.less -->
                {{-- @include('includes.parts.header.notifications') --}}

                <!-- Tasks: style can be found in dropdown.less -->
                {{-- @include('includes.parts.header.tasks') --}}

                <!-- User Account: style can be found in dropdown.less -->
                @include('includes.parts.header.user')
                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>