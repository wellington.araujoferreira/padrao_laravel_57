<meta charset="utf-8">
{{-- <link rel="shortcut icon" href="{{{ asset('img/favicons/favicon.png') }}}"> --}}
<link rel="shortcut icon" href="{{ asset('img/favicons/favicon.ico') }}">
<link href="{{{ asset('css/style.css') }}}" rel="stylesheet">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
{{-- titulo da pagina --}}
<title>{{config('app.name') . config('app.type')}}</title>