  {{-- <section class="content"> --}}
    <div class="box box-info box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">
          <i class="fas fa-birthday-cake"></i>
          Aniversariantes do dia
        </h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
        </div>
        <!-- /.box-tools -->
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-striped">
          <tbody>
            <tr>
              <td><i class="fas fa-angle-double-right"></i></td>
              <td>04/05/1969</td>
              <td>Wellington de Araújo Ferreira</td>
              <td>Agente Penitenciário</td>
            </tr>
            <tr>
              <td><i class="fas fa-angle-double-right"></i></td>
              <td>04/05/1969</td>
              <td>Wellington de Araújo Ferreira</td>
              <td>Agente Penitenciário</td>
            </tr>
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
  {{-- </section> --}}