@php
  $fields = new App\Classes\Fields;

  $total = $fields->getNumPessoa();
  $agGrupoPercent = $fields->getPercent(2);
  $edMdGrupoPercent= $fields->getPercent(3);
  $edSpGrupoPercent = $fields->getPercent(4);

  $agGrupoTotal = $fields->getNumPessoaCargo(2);
  $edMdGrupoTotal = $fields->getNumPessoaCargo(3);
  $edSpGrupoTotal = $fields->getNumPessoaCargo(4);


  $totalSindicalizado = $fields->getNumPessoa(1);
  $agente = $fields->getPercent(2,1);
  $edmedio = $fields->getPercent(3,1);
  $edsuperior = $fields->getPercent(4,1);

  $agSindTotal = $fields->getNumPessoaCargo(2,1);
  $edMdSindTotal = $fields->getNumPessoaCargo(3,1);
  $edSpSindTotal = $fields->getNumPessoaCargo(4,1);

  $sindicalizadoPencentual = ($totalSindicalizado * 100) / $total;
  $sindPencent = number_format($sindicalizadoPencentual, 2, ',', '.');

  // $edsuperior = $fields::getCountPessoa(4);
@endphp
{{-- Mostra dados de todo grupo penitenciario --}}
<div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $total }}</h3>

          <p>Grupo Penitenciário</p>
        </div>
        <div class="icon">
          <i class="ion ion-male"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $agGrupoPercent }}<sup style="font-size: 20px">%</sup></h3>

          <p>{{ $agGrupoTotal }}: Agentes Penitenciários</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $edMdGrupoPercent }}<sup style="font-size: 20px">%</sup></h3>

          <p>{{ $edMdGrupoTotal }}: Educador Nível Médio</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $edSpGrupoPercent }}<sup style="font-size: 20px">%</sup></h3>

          <p>{{ $edSpGrupoTotal }}: Educador Nível Superior</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
</div>

{{-- Mostra dados sindicalizados --}}

<div class="row">
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h3>{{ $totalSindicalizado }}</h3>

          <p>{{ $sindPencent }}% Sindicalizados</p>
        </div>
        <div class="icon">
          <i class="ion ion-male"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h3>{{ $agente }}<sup style="font-size: 20px">%</sup></h3>

          <p>{{ $agSindTotal }}: Agentes Penitenciários</p>
        </div>
        <div class="icon">
          <i class="ion ion-stats-bars"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h3>{{ $edmedio }}<sup style="font-size: 20px">%</sup></h3>

          <p>{{ $edMdSindTotal }}: Educador Nível Médio</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h3>{{ $edsuperior }}<sup style="font-size: 20px">%</sup></h3>

          <p>{{ $edSpSindTotal }}: Educador Nível Superior</p>
        </div>
        <div class="icon">
          <i class="ion ion-pie-graph"></i>
        </div>
        <a href="#" class="small-box-footer">Mais informações <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
</div>