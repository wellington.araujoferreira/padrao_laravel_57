<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-success col-sm-10">
                <div class="panel-heading">{{ config('app.sigla') }} Macapá-Ap, {{ date('d/m/Y H:i:s')}}</div>

                <div class="panel-body">
                    Seja bem vindo, {{ Auth::user()->name }}!
                </div>
                <div class="panel-body">
                    Bom trabalho!
                </div>
            </div>
        </div>
    </div>
</div>