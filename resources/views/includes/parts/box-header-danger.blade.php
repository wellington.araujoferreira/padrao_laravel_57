<div class="box box-solid box-warning">
	<div class="box-header with-border">
		<h3 class="box-title">
			@yield('action', 'Título da Ação')
		</h3>
	<div class="box-body">
		@yield('content', 'Conteúdo')
	</div>
	<!-- /.box-body -->
	<div class="box-footer text-center">
		@yield('footer')
	</div>
	<!-- /.box-footer-->
</div>