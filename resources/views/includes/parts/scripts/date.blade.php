{{-- plugin de data calendario --}}
{{-- este habilita o calendário mas o mesmo vem com as legendas em portugues --}}
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
{{-- este outro traduz as legenda do calendario para o portugues do brasil --}}
<script src="{{asset('js/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>

<script>
	$(document).ready(function () {
	  	var currentDate = new Date();
	    $('.date').datepicker({
	    	// defaultDate: currentDate,
	      format: "dd/mm/yyyy",
	    	language: 'pt-BR',
	    	weekStart: 0,
	    	// startDate:'0d',
	    	todayHighlight: true
	    });

	    $('.date_initial').datepicker({
			// defaultDate: currentDate,
	      	format: "dd/mm/yyyy",
	    	language: 'pt-BR',
	    	weekStart: 0,
	    	// startDate:'0d',
	    	todayHighlight: true
	    });

		  $('.date_finish').datepicker({
			// defaultDate: currentDate,
	      	format: "dd/mm/yyyy",
	    	language: 'pt-BR',
	    	weekStart: 0,
	    	// startDate:'0d',
	    	todayHighlight: true
	    });
	});
</script>