<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('dist/js/adminlte.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/fastclick/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('dist/js/app.min.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ asset('dist/js/demo.js') }}"></script>
{{-- plugin de data calendario --}}
<script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
{{-- invocar o sweetalert --}}
<script type="text/javascript" src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
<script src="{{asset('js/locales/bootstrap-datepicker.pt-BR.min.js')}}"></script>
<script src="{{ asset('js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ asset('js/maskMoney.js') }}"></script>
<script src="{{ asset('js/select2.full.min.js') }}"></script>
<script defer src="{{ asset('js/fontawesome-all.min.js') }}"></script>

@yield('alert')

<script type="text/javascript">
	$(function(){
		$('.btn-alert').on('click', function(){
			//pega o formulário
			var form = $(this).parents('form');

			swal({
				title: "Excluir o registro?",
				text: "Este registro será excluído definitivamente!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Sim, excluir!",
				closeOnConfirm: false
			},
			function(){
				//executa o submit do formulario
				form.submit();
			});
		});

	});
</script>

<script>
  $(document).ready(function () {
  	var currentDate = new Date();

    $('#date_initial').datepicker({
		// defaultDate: currentDate,
      	format: "dd/mm/yyyy",
    	language: 'pt-BR',
    	weekStart: 0,
    	// startDate:'0d',
    	todayHighlight: true
    });

	  $('#date_finish').datepicker({
		// defaultDate: currentDate,
      	format: "dd/mm/yyyy",
    	language: 'pt-BR',
    	weekStart: 0,
    	// startDate:'0d',
    	todayHighlight: true
    });

  $('.select_search').select2();

  $('[data-url]').on('change', function(){
      var url= $(this).data('url');
      var id = $(this).val();
      var input = $(this).data('input');
      var label = $(this).data('label');
      var append = $(this).data('append');
      console.log(id);
      $.ajax({
         url: url + "?"+input+ "=" + id,
      }).done(function( data ) {
        var option = '<option value="0">Selecione a unidade</option>';
        console.log(append);
         for (var i = 0; i < data.length; i++) {
          option +='<option value="'+data[i].id+'">'+eval("data[i]."+label)+'</option>';
         }
         $(append).find('option').remove();
         $(append).append(option);
        });
      });
  });

      /**
     * [Masked Mascara de campos]
     * @type {String}
     */
    $('#cnpj').mask('99.999.999/9999-99', {placeholder: "#"});
    $('#cpf').mask('999.999.999-99', {placeholder: "#"});
    $('#cep').mask('99999-999', {placeholder: "#"});
    $('#fone').mask('(99)99999-9999', {placeholder: "#"});
    $('#fixo').mask('(99)9999-9999', {placeholder: "#"});
    $('#celular').mask('(99)99999-9999', {placeholder: "#"});
    $('#contato').mask('(99)99999-9999', {placeholder: "#"});
    $('#hora').mask('99:99');
    $('#hora_inicial').mask('99:99');
    $('#hora_final').mask('99:99');
    $('#data').mask('99/99/9999');

    $('.hora').mask('99:99');

  $('#numeric').keyup(function() {
    $(this).val(this.value.replace(/\D/g, ''));
  });

  $("input.moeda").maskMoney({
        showSymbol:true,
        symbol:"",
        decimal:",",
        thousands:".",
        allowNegative:true
  });
</script>

<script>
  $("#fileUpload").on('change', function () {
      if (typeof (FileReader) != "undefined") {
          var image_holder = $("#image-holder");
          image_holder.empty();
          var reader = new FileReader();
          reader.onload = function (e) {
              $("<img />", {
                  "src": e.target.result,
                  "class": "thumb-image",
                  "style": "width: 150px;height: 150px"
              }).appendTo(image_holder);
          }
          image_holder.show();
          reader.readAsDataURL($(this)[0].files[0]);
      } else{
          alert("Este navegador nao suporta FileReader.");
      }
  });
</script>
