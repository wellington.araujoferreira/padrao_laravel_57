@php
  $fields = new App\Classes\Fields;
  $model = $fields->getBirtday();
@endphp

{{-- <section class="content"> --}}
  <div class="box box-info box-solid">
    <div class="box-header with-border">
      <h3 class="box-title">
        <i class="fas fa-birthday-cake"></i>
        Aniversariantes de hoje <strong class="text-color-cyan">{{ date('d/m/Y') }}</strong>
      </h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
      </div>
      <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
      <table class="table table-striped">
        <tbody>
          @foreach ($model as $element)
            <tr>
              <td><i class="fas fa-angle-double-right"></i></td>
              <td>{{ $element->nome }}</td>
              <td>{{ $element->cargo->descricao }}</td>
              <td>
                @if ($element->sindicalizado)
                  <i class="far fa-thumbs-up"></i>
                @else
                  <i class="far fa-thumbs-down"></i>
                @endif
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <!-- /.box-body -->
  </div>
{{-- </section> --}}