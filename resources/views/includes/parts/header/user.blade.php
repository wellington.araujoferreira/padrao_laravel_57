@if (Auth::guest())
	<li><a href="{{ route('login') }}">Login</a></li>
    <li><a href="{{ route('register') }}">Register</a></li>
@else
	<li class="dropdown user user-menu">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
			@if (!is_null(Auth::user()->img))
				<img src="data:{{ Auth::user()->type }};base64, {{ Auth::user()->img }}" class="user-image" alt="User Image">
			@else
				<i class="far fa-user-circle"></i>
			@endif
			{{-- <img src="data:{{ Auth::user()->type }};base64, {{ Auth::user()->img }}" class="user-image" alt="User Image"> --}}
			<span class="hidden-xs">{{ Auth::user()->name }}</span>
		</a>
		<ul class="dropdown-menu">
			<!-- User image -->
			<li class="user-header">
				@if (!is_null(Auth::user()->img))
					<img src="data:{{ Auth::user()->type }};base64, {{ Auth::user()->img }}" class="img-circle" alt="User Image">
				@else
					<i class="far fa-user-circle fa-4x text-color-white"></i>
				@endif

				<p>
					{{Auth::user()->name}}
					@if (Auth::user()->cpf == '32517394253')
						<small>Desenvolvedor do siapen</small>
					@endif
				</p>
			</li>
			<li class="user-footer">
				<div class="pull-left">
					<a href="{{ route('user.show', Auth::user()->cpf) }}" class="btn btn-default">
						<i class="fa fa-unlock"></i>
						Trocar Senha
					</a>
				</div>
				{{-- <div class="pull-left">
					<a href="#" class="btn btn-default btn-flat">Profile</a>
				</div> --}}
				<div class="pull-right">
					{{-- <a href="{{ route('logout') }}" class="btn 	btn-default btn-flat">
						<i class="fa fa-sign-out"></i>
						Sair
					</a> --}}
					<a href="{{ route('logout') }}"
					onclick="event.preventDefault();
					document.getElementById('logout-form').submit();" class="btn btn-dark">
						<i class="fas fa-sign-out-alt"></i>
						Sair
					</a>
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</div>
			</li>
		</ul>
	</li>
@endif
