  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        {{-- legenda de grupo de menus --}}
        @inject('area', 'App\Area')
        @php
            $areas = $area::orderBy('ordem')->get();
        @endphp

        @foreach ($areas as $a)
            {{-- expr --}}
            <li class="header">{{$a->name}}</li>
            @foreach ($a->menus as $m)
                <li class="treeview">
                    <a href="#" >
                      <i class="fa {{ $m->icon }}"></i><span>{{ $m->name }}</span>
                    </a>
                    @foreach ($m->submenus as $s)
                        @if (Auth::user()->hasRules($s->route))
                          {{-- Exite a rota --}}
                        @else
                          {{-- Não existe a rota --}}
                        @endif
                        <ul class="treeview-menu">
                          {{ Html::item($s->route, $s->name, $s->icon, $s->description) }}
                        </ul>
                    @endforeach
                </li>
            @endforeach
        @endforeach
    </ul>
</section>
<!-- /.sidebar -->
</aside>
