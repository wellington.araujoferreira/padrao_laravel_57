<footer class="main-footer">
	<div class="navbar  navbar-fixed-bottom">
		<div class="box-footer text-center">
			<strong class="blocktext">@yield('footer', config('app.entity') )</strong>
			<strong class="strong-rodape">Copyright &copy; 2014-2016 <a href="#">Wellington de Araújo Ferreira</a>.</strong> All rights reserved.
		</div>
	</div>
</footer>