<div class="box box-solid {{ config('app.box-theme') }}">
	<div class="box-header with-border">
		<h3 class="box-title">
			@yield('action', 'Título da Ação')
		</h3>

		<div class="box-tools pull-right">
			@yield('plus')
			<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Esconder...">
				<i class="fa fa-minus"></i>
			</button>
			<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Fechar..." disabled="true">
					<i class="fa fa-times"></i>
			</button>
		</div>
	</div>

	<div class="box-body form-horizontal">
		<div class="box-body">
			@yield('content', 'Conteúdo')
		</div>
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<div class="text-center">
			@if (isset($models))
				{{ $models->appends(request()->except('page'))->links() }}
			@endif

		</div>
		<div class="text-center text-color-blue">
			@yield('footer', config('app.entity') . ' - ' . config('app.sigla'))
		</div>
	</div>
	<!-- /.box-footer-->
</div>