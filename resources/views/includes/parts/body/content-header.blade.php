<section class="content-header">
    <h1>
        @yield('title', 'Título da pagina')
        <small>@yield('description')</small>
    </h1>
</section>