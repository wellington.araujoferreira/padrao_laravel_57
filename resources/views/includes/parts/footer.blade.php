<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>

@yield('alert')

<script type="text/javascript">
	$(function(){
		$('.btn-alert').on('click', function(){
			//pega o formulário 
			var form = $(this).parents('form');

			swal({
				title: "Excluir o registro?",
				text: "Este registro será excluído definitivamente!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Sim, excluir!",
				closeOnConfirm: false
			},
			function(){
				//executa o submit do formulario
				form.submit();
			});
		});
	});
</script>
