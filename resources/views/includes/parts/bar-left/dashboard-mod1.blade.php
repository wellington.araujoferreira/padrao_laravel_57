
	@inject('menu','App\Menu')
	@php
		$menus = $menu::orderBy('id')->get();
	@endphp
	@foreach ($menus as $m)
		<li class="treeview">
			<a href="#" >
				<i class="fa {{ $m->icon }}"></i><span>{{ $m->name }}</span>
				<span class="pull-right-container">
					<span class="label label-primary">{{ $m->submenus->count() }}</span>
				</span>
			</a>
			@foreach ($m->submenus as $s)
				<ul class="treeview-menu">
					{{ Html::item($s->route, $s->name, $s->icon, $s->description) }}
				</ul>
			@endforeach
		</li>
	@endforeach
