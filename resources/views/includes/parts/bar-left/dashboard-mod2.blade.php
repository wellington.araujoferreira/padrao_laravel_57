<li class="treeview">
	<a href="#">
		<i class="fa fa-files-o"></i>
		<span>Atendimento</span>
		<span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>
			<span class="label label-primary pull-right">4</span>
		</span>
	</a>
	<ul class="treeview-menu">
		<li><a href="../layout/top-nav.html"><i class="fa fa-circle-o"></i> Top Navigation</a></li>
		<li><a href="../layout/boxed.html"><i class="fa fa-circle-o"></i> Boxed</a></li>
		<li><a href="../layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
		<li><a href="../layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
	</ul>
</li>
