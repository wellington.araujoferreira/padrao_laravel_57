
@if (Auth::user()->hasRules($route))
	<li>
		<a href="{{ route($route) }}" data-toggle="tooltip" title="">
		<i class="fa {{ $icon }}"></i> {{ $label }}</a>
@endif