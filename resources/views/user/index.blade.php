@extends('layouts.app')

@section('title')
	Usuários do sistema
@stop

@section('description')
	Cadastro de usuários do sistema
@stop

@section('action')
	Listagem dos usuários cadastrados
@stop

@if (Auth::user()->hasRules('user.create'))
	@section('plus')
		<a href="{{ url('/register') }}" class="btn btn-info" data-toggle="tooltip" title="Novo usuário">
			<i class="fa fa-bolt"></i>
		</a>
	@stop
@endif

@section('content')

	<div class="well">
		@include('user.search')
	</div>

	<table class='table table-hover table-responsive-lg'>
		<thead>
			<tr class="">
				<th>#</th>
				<th>Nome</th>
				<th>CPF</th>
				<th>E-mail</th>
				<th></th>
			</tr>
		</thead>

		@foreach ($models as $m)
			<tbody>
				<tr style="font-size: 11px">
					<td>{{ $m->id }}</td>
					<td>{{ $m->name }}</td>
					<td>{{ $m->cpf }}</td>
					<td>{{ $m->email }}</td>
					<td width="10%">
						@include('includes.helper.action', ['model'=>$m, 'route'=>'user'])
					</td>
				</tr>
			</tbody>
		@endforeach
	</table>
@stop
