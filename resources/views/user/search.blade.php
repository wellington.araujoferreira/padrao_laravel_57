
{{ Form::open([ 'route' => 'user.index', 'method'=>'GET', 'class'=>'form-inline']) }}
	{!! csrf_field() !!}

	<div class="box box-solid {{ config('app.box-theme') }}">
		<div class="box-header with-border">
			<h3 class="box-title">
				<i class="fa fa-search"></i>
				Buscar
			</h3>
			<div class="box-tools pull-right">
				<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Esconder...">
					<i class="fa fa-minus"></i>
				</button>
				<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Fechar..." disabled="true">
						<i class="fa fa-times"></i>
				</button>
			</div>
		</div>

		<div class="box-body form-horizontal">
			<div class="box-body">
				<div class="form-group">
					{{-- {{ Form::label('id', 'Registro', ['class'=>'control-label col-sm-2']) }} --}}
					<div class="col-sm-1">
						{{ Form::text('id', $id, ['class'=>'form-control', 'placeholder'=>'ID','title'=>'Nº de registro da avaliação social']) }}
					</div>
					<div class="col-sm-4">
						{{ Form::text('name', $name, ['class'=>'form-control', 'title'=>'Nome do usuário', 'placeholder' => 'Nome', 'style'=>'width:100%']) }}
					</div>
					<div class="col-sm-2">
						{{Form::text('cpf', $cpf, ['class'=>'form-control', 'title'=>'Número do CPF somente números', 'placeholder'=> 'CPF'])}}
					</div>
					<div class="col-sm-2">
						{{Form::text('email', $email, ['class'=>'form-control', 'title'=>'Endereço de email', 'placeholder'=> 'email@email.com'])}}
					</div>
					<div class="col-sm-3">
						<div class="btn-group role="group" aria-label="Basic example">
							<a href="{{ Route('user.index') }}" class="form-control btn btn-default btn-sm">	<i class="fa fa-trash"></i>Limpar</a>
							{{ Form::button('<i class="fa fa-search"></i> Buscar', ['type'=>'submit', 'class'=>'form-control btn-primary btn-sm']) }}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
{{ Form::close() }}