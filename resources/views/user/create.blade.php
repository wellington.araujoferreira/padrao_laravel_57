@extends('layouts.app')

@section('title')
	Usuários do sistema
@stop

@section('description')
	Cadastro dos usuários do sistema
@stop

@section('action')
	Dados do usuário
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			'route' => '/register',
			'enctype' => "multipart/form-data",
		];
		// Caso haja, muda a rota e chama o metodo update passando como parametro o id do registro a ser editado
		if (isset($model))
			$action = [
				// rota a ser chamada
				'route' => ['user.update', $model->id],
				// invoca o metodo PUT
				'method' => 'PUT',
				'enctype' => "multipart/form-data",
			];

	@endphp

	{{ Form::open($action) }}
		{!! csrf_field() !!}
		{{-- caso exista o model, prepara o formulario com os dados do registro passado pelo id --}}
		@if (isset($model))
			@php
				Form::setModel($model);
			@endphp
		@endif

	<div class="row">
		{{-- Lado esquerdo foto --}}
		<div class="col-md-4 well">
			<div class="col-sm-8">
				<a href="#" class="thumbnail">
				  @if (isset($model->img))
				  	<img src="data:{{ $model->type }};base64, {{ $model->img }}" alt="Foto do usuário" name="img" value="{{ $model->img }}" alt="Foto do usuário" class="img-40-40" />
				  @else
				  	<img src="{{ asset('img/avatar5.png') }}" alt="Foto do usuário" class="img-40-40" />
				  @endif
				</a>
				{{ Form::file('imagem', null, ['class'=>'form-control control-file-input', 'id'=>'file']) }}
			</div>
		</div>

		{{-- Lado direito dados do usuário --}}
		<div class="col-md-8">

			<div class="control-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fas fa-user"></i></span>
					{{ Form::text('name', null, ['class'=>'form-control', 'title'=>'Informe o nome do usuário']) }}
				</div>
			</div>

			<div class="control-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fas fa-calendar"></i></span>
					{{ Form::text('data', null, ['class'=>'form-control date', 'title'=>'Informe o nome do usuário']) }}
				</div>
			</div>

			<div class="control-group">
				<div class="input-group">
					<span class="input-group-addon">12</span>
					{{ Form::text('cpf', null, ['class'=>'form-control', 'id'=>'cpf', 'title'=>'Informe o número do cpf']) }}
				</div>
			</div>

			<div class="control-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="far fa-envelope"></i></span>
					{{ Form::text('email', null, ['class'=>'form-control', 'id'=>'email', 'title'=>'Informe o e-mail do usuário']) }}
				</div>
			</div>

			@inject('profile', 'App\Profile')
			@php
				$profileOptions = $profile::pluck('name', 'id');
				$profile_ids 	= $model->profiles()->pluck('profiles.id', 'profiles.id')->toArray();
			@endphp

			<div class="control-group">
				<div class="input-group">
					<span class="input-group-addon"><i class="fa fa-check"></i></span>
					{{ Form::select('profile_id', $profileOptions,  $profile_ids,['class'=>'form-control', 'multiple'=>true, 'title'=>'Selecione um mais perfil para o usuários']) }}
				</div>
			</div>

            <div class="control-group{{ $errors->first('password') ? ' has-error' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                    <input type="password" name="password" id="password" class="form-control" title="Informe a senha">
                </div>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

			<div class="control-group">
				<a href="{{ route('user.index') }}" class="btn btn-default">
					<i class="fa fa-arrow-left"></i>
					Voltar
				</a>
				{{ Form::submit('enviar', ['class'=>'btn btn-success btn-save']) }}
			</div>
		</div>
	</div>

	{{ Form::close() }}
@stop

@section('script')
	@include('includes.parts.scripts.date')
@stop

@section('footer')
	{{-- <div class="alert alert-warning">Atenção</div> --}}
@stop