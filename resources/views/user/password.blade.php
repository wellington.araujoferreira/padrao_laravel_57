@extends('layouts.app')

@section('title')
	{{ Auth::user()->name }}
@stop

@section('description')
	CPF: {{ Auth::user()->cpf }}
@stop

@section('action')
	Trocar senha
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			// rota a ser chamada
			'route' => ['user.update', $model->id],
			// invoca o metodo PUT
			'method' => 'PUT',
			'enctype' => "multipart/form-data",
		];
	@endphp

	{{ Form::open($action) }}
			{!! csrf_field() !!}
		@if (isset($model))
			@php
				Form::setModel($model);
			@endphp
		@endif

		<div class="row">
			{{-- Lado direito dados do usuário --}}
			<div class="col-md-6">

				{{ Form::hidden('id') }}
				<div class="form-group">
					{{ Form::label('passOld', 'Senha Atual', ['class'=>'control-label col-sm-4']) }}
					<div class="col-sm-8">
						<div class="input-group">
							{{ Form::password('passOld',  null, ['class'=>'form-control']) }}
						</div>
					</div>
				</div>

				<div class="form-group">
					{{ Form::label('newPassword', 'Nova Senha', ['class'=>'control-label col-sm-4']) }}
					<div class="col-sm-8">
						<div class="input-group">
							{{ Form::password('newPassword',  null, ['class'=>'form-control']) }}
						</div>
					</div>
				</div>

				<div class="form-group">
					{{ Form::label('password', 'Repita a nova senha', ['class'=>'control-label col-sm-4']) }}
					<div class="col-sm-8">
						<div class="input-group">
							{{ Form::password('password',  null, ['class'=>'form-control']) }}
						</div>
					</div>
				</div>

				<div class="control-group">
					<a href="{{ route('user.index') }}" class="btn btn-default">
						<i class="fa fa-arrow-left"></i>
						Voltar
					</a>
					{{ Form::button('<i class="fa fa-save"></i> Trocar', ['class'=>'btn btn-success', 'type'=>'submit']) }}
				</div>
			</div>
		</div>

	{{ Form::close() }}
@stop
