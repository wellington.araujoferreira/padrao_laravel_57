@extends('layouts.app')

@section('title')
    <strong class="text-color-new-blue">{{config('app.name')}}</strong>{{config('app.type')}}
@stop

@section('description')
    {{config('app.description')}}
@stop

@section('action')
    Área de trabalho
@stop

@section('content')
<div class="container">
    <section class="content">
      {{-- @include('includes.parts.dashboard.dashboard') --}}
      <div class="row">
        <!-- Left col -->
		@if ( config('app.dashboard_enabled'))
			{{-- expr --}}
	        <section class="col-lg-7 connectedSortable">
	            @include('includes.parts.dashboard.agenda')
	            @include('includes.parts.dashboard.birtmonth')
	        </section>

	        <section class="col-lg-5 connectedSortable">
	            @include('includes.parts.dashboard.birtday')
	        </section>
        @endif
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
</div>
@endsection
