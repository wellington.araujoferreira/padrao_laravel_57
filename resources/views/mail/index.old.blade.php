@extends('layouts.lte')
@section('title')
	Mala direta
@stop

@section('description')
	Envio de e-mail
@stop

@section('action')
	Dados da mensagem
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			'route' => 'mail.store'
		];

	@endphp

	{{ Form::open($action) }}
		{!! csrf_field() !!}

	<div class="form-group">
		{{ Form::label('usuario', 'Nome do usuário') }}
		{{ Form::text('usuario', null, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		{{ Form::label('email', 'Conta de e-mail') }}
		{{ Form::text('email', null, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		{{ Form::label('mensagem', 'Mensagem') }}
		{{ Form::text('mensagem', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		<a href="{{ route('home') }}" class="btn btn-default">
			<i class="fa fa-arrow-left"></i>
			Voltar
		</a>
		{{ Form::submit('Enviar', ['class'=>'btn btn-success btn-save']) }}
	</div>

	{{ Form::close() }}
@stop
