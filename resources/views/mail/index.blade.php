@extends('layouts.app')

@section('title')
	Menus do sistema
@stop

@section('description')
	Cadastro de Menus do sistema
@stop

@section('action')
	Listagem das Menus cadastradas
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			'route' => 'mail.store'
		];

	@endphp

	{{ Form::open($action) }}
		{!! csrf_field() !!}

	<div class="form-group">
		{{ Form::label('usuario', 'Nome do usuário') }}
		{{ Form::text('usuario', null, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		{{ Form::label('email', 'Conta de e-mail') }}
		{{ Form::text('email', null, ['class'=>'form-control']) }}
	</div>
	<div class="form-group">
		{{ Form::label('mensagem', 'Mensagem') }}
		{{ Form::text('mensagem', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		<a href="{{ route('home') }}" class="btn btn-default">
			<i class="fa fa-arrow-left"></i>
			Voltar
		</a>
		{{ Form::submit('Enviar', ['class'=>'btn btn-success btn-save']) }}
	</div>

	{{ Form::close() }}
@stop