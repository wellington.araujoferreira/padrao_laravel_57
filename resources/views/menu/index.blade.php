@extends('layouts.app')

@section('title')
	Menus do sistema
@stop

@section('description')
	Cadastro de Menus do sistema
@stop

@section('action')
	Listagem das Menus cadastradas
@stop

@if (Auth::user()->hasRules('menu.store'))
	@section('plus')
		<a href="{{ route('menu.create') }}" class="btn btn-info" data-toggle="tooltip" title="Novo Menu">
			<i class="fa fa-bolt"></i>
		</a>
	@stop
@endif

@section('content')
<table class='table table-responsive table-condensed table-hover'>
	<tr>
		<th>#</th>
		<th>Área</th>
		<th>Nome</th>
		<th>Description</th>
		<th>Icone</th>
		<th><i class="fa fa-arrow-down"></i></th>
		<th></th>
	</tr>

	{!! csrf_field() !!}
	@foreach ($models as $m)
		<tr>
			<td>{{ $m->id }}</td>
			<td>{{ $m->area->name }}</td>
			<td>{{ $m->name }}</td>
			<td>{{ $m->description }}</td>
			<td>{{ $m->icon }}</td>
			<td><i class="fa {{ $m->icon }}"></i></td>
			<td width="12%">
				@include('includes.helper.action', ['model'=>$m, 'route'=>'menu'])
			</td>
		</tr>
	@endforeach
</table>
@stop

{{-- Paginacao --}}
@section('footer')
	<div class="text-center">
		{{ $models->links() }}
	</div>
@stop