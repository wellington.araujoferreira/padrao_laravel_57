@extends('layouts.app')

@section('title')
	Menus do sistema
@stop

@section('description')
	Cadastro de Menu dos post
@stop

@section('action')
	Dados do Menu
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			'route' => 'menu.store'
		];
		// Caso haja, muda a rota e chama o metodo update passando como parametro o id do registro a ser editado
		if (isset($model))
			$action = [
				// rota a ser chamada
				'route' => ['menu.update', $model->id],
				// invoca o metodo PUT 
				'method' => 'PUT'
			];

	@endphp

	{{ Form::open($action) }}
		{!! csrf_field() !!}
		{{-- caso exista o model, prepara o formulario com os dados do registro passado pelo id --}}
		@if (isset($model))
			@php
				Form::setModel($model);
			@endphp
		@endif
	@inject('area', 'App\Area')
	@php
		$options = $area::pluck('name', 'id')->toArray();
	@endphp

	<div class="form-group">
		{{ Form::label('area_id', 'Area') }}
		{{ Form::select('area_id', $options, null, ['class'=>'form-control']) }}
	</div>	

	<div class="form-group">
		{{ Form::label('name', 'Nome da menu') }}
		{{ Form::text('name', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('description', 'Descrição do menu') }}
		{{ Form::text('description', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('icon', 'Ícone do menu') }}
		{{ Form::text('icon', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		<a href="{{ route('menu.index') }}" class="btn btn-default">
			<i class="fa fa-arrow-left"></i>
			Voltar
		</a>
		{{ Form::submit('enviar', ['class'=>'btn btn-success btn-save']) }}
	</div>

	{{ Form::close() }}
@stop

@section('footer')
	{{-- <div class="alert alert-warning">Atenção</div> --}}
@stop