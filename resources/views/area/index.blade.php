@extends('layouts.app')

@section('title')
	Áreas do sistema
@stop

@section('description')
	Cadastro de Áreas do sistema
@stop

@section('action')
	Lista das Áreas cadastradas
@stop

@if (Auth::user()->hasRules('area.store'))
	@section('plus')
		<a href="{{ route('area.create') }}" class="btn btn-info" data-toggle="tooltip" title="Nova Area">
			<i class="fa fa-bolt"></i>
		</a>
	@stop
@endif


@section('content')
<table class='table table-responsive table-condensed table-hover'>
	{!! csrf_field() !!}
	<tr>
		<th>#</th>
		<th>Nome</th>
		<th>Descrição</th>
		<th>Cor da fonte</th>
		<th>Ordem</th>
		<th></th>
	</tr>

	@php
		//dd($models);
	@endphp

	@foreach ($models as $m)
		<tr>
			<td>{{ $m->id }}</td>
			<td>{{ $m->name }}</td>
			<td>{{ $m->description }}</td>
			<td>{{ $m->color }}</td>
			<td>{{ $m->ordem }}</td>
			<td width="12%">
				@include('includes.helper.action', ['model'=>$m, 'route'=>'area'])
			</td>
		</tr>
	@endforeach
</table>
@stop 