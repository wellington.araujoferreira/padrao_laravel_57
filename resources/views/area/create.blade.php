@extends('layouts.app')

@section('title')
	Areas do sistema
@stop

@section('description')
	Cadastro de Area do sistema
@stop

@section('action')
	Dados da Area
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			'route' => 'area.store'
		];
		// Caso haja, muda a rota e chama o metodo update passando como parametro o id do registro a ser editado
		if (isset($model))
			$action = [
				// rota a ser chamada
				'route' => ['area.update', $model->id],
				// invoca o metodo PUT 
				'method' => 'PUT'
			];

	@endphp

	{{ Form::open($action) }}
		{!! csrf_field() !!}
		{{-- caso exista o model, prepara o formulario com os dados do registro passado pelo id --}}
		@if (isset($model))
			@php
				Form::setModel($model);
			@endphp
		@endif

	<div class="form-group">
		{{ Form::label('name', 'Nome da Area') }}
		{{ Form::text('name', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('description', 'Descrição do Area') }}
		{{ Form::text('description', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		{{ Form::label('color', 'Cor da fonte') }}
		{{ Form::text('color', null, ['class'=>'form-control']) }}
	</div>	

	<div class="form-group">
		{{ Form::label('ordem', 'Ordem de apresentação') }}
		{{ Form::text('ordem', null, ['class'=>'form-control']) }}
	</div>

	<div class="form-group">
		<a href="{{ route('area.index') }}" class="btn btn-default">
			<i class="fa fa-arrow-left"></i>
			Voltar
		</a>
		{{ Form::submit('enviar', ['class'=>'btn btn-success btn-save']) }}
	</div>

	{{ Form::close() }}
@stop

@section('footer')
	{{-- <div class="alert alert-warning">Atenção</div> --}}
@stop