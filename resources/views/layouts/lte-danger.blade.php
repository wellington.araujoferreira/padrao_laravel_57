<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    {{-- titulo da pagina --}}
    <title>{{config('app.name')}}</title>
    {{-- Inlcusão dos estilos --}}
    @include('includes.parts.style')
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        @include('includes.helper.alert')
        @if (Auth::check())
            @include('includes.parts.header')
            <!-- =============================================== -->
            {{-- barra lateral direita - aside-left --}}
            <!-- =============================================== -->
            @include('includes.parts.aside-left')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                @include('includes.parts.content-header')

                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    @include('includes.parts.box-header-danger')
                    <!-- /.box -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            @include('includes.parts.rodape')
            {{-- barra lateral direita - asidebar-right --}}
            @include('includes.parts.asidebar-right')
            <!-- ./wrapper -->
        @endif
    </div>
    {{-- Incluindo os scrips jquery --}}
    @include('includes.parts.script')
</body>
</html>
