<!DOCTYPE html>
<html lang="en">
<head>	
	<title>{{ config('app.name') . config('app.type')  . ' ' . config('app.version') }}</title>
	@include('includes.parts.head')
</head>
<body>

	@include('includes.menu.nav')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>@yield('title')</h1>
				@include('includes.helper.alert')
				@yield('content')
			</div>
		</div>
	</div>
	{{-- @include('includes.parts.rodape') --}}
	{{-- @include('includes.parts.footer'); --}}
	@include('includes.parts.script')
</body>
</html>