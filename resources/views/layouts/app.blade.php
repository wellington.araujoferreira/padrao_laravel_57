<!DOCTYPE html>
<html lang="en">
<head>
	{{-- Configurações de heads --}}
	@include('includes.parts.head.default')

	{{-- Inlcusão dos estilos --}}
    @include('includes.parts.head.style')

	<title>{{ config('app.name') . config('app.type')  . ' ' . config('app.version') }}</title>
</head>
<body class="hold-transition {{ config('app.skin-theme') }} sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        @include('includes.helper.alert')
        @if (Auth::check())
            @include('includes.parts.body.nav-bar-top')
            <!-- =============================================== -->
            {{-- barra lateral direita - aside-left --}}
            <!-- =============================================== -->
            @include('includes.parts.body.aside-left')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                @include('includes.parts.body.content-header')

                <!-- Main content -->
                <section class="content">
                    <!-- Default box -->
                    @include('includes.parts.body.box-header')
                    <!-- /.box -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            @include('includes.parts.body.footer')
            {{-- barra lateral direita - asidebar-right --}}
            @include('includes.parts.asidebar-right')
            <!-- ./wrapper -->
        @endif
    </div>
    {{-- Incluindo os scrips jquery --}}
    @include('includes.parts.scripts.default')
    @section('script')
    @show
</body>
</html>