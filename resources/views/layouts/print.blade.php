<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        @include('includes.parts.header.print')
    </head>
    <body>
        <section class="content">
            @include('includes.parts.body.table')
        </section>
    </body>
</html>
@include('includes.parts.rodape');