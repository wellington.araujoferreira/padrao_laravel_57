@extends('layouts.app')
@section('title')
	Perfil de usuario
@stop

@section('description')
	Concessão de permissões para usuários
@stop

@section('action')
	Lista de perfis cadastrados
@stop

@if (Auth::user()->hasRules('profile.store'))
	@section('plus')
		<a href="{{ route('profile.create') }}" class="btn btn-info" data-toggle="tooltip" title="Novo perfil">
			<i class="fa fa-bolt"></i>
		</a>
	@stop
@endif

@section('content')
<table class='table table-responsive table-condensed table-hover'>
	<tr>
		<th>Id</th>
		<th>Nome do perfil</th>
		<th></th>
	</tr>

	@php
		//dd($models);
	@endphp
	{!! csrf_field() !!}
	@foreach ($models as $m)
		<tr>
			<td>{{ $m->id }}</td>
			<td>{{ $m->name }}</td>
			<td width="12%">
				@include('includes.helper.action', ['model'=>$m, 'route'=>'profile'])
			</td>
		</tr>
	@endforeach
</table>
{{-- Paginacao --}}
<div class="text-center">
	{{-- {{ $models->links() }} --}}
</div>
@stop 

