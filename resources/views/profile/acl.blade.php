@php
	$acl = config('acl');
	$rules = [];

	if(isset($model)){
		$rules = $model->rules()->pluck('route', 'route')->toArray();
	}

	//dd($rules);
@endphp

<table class="table table-striped">
	<thead>
		<tr>
			<th>Controller</th>
			<th>Listagem</th>
			<th>Criar</th>
			<th>Editar</th>
			<th>Excluir</th>
		</tr>
	</thead>
	<tbody>
			{!! csrf_field() !!}
		@foreach($acl as $chave => $data)
			@php
				$index = array_key_exists($chave. '.index', $rules) ? 'checked' : '';
				$store = array_key_exists($chave. '.store', $rules) ? 'checked' : '';
				$update = array_key_exists($chave. '.update', $rules) ? 'checked' : '';
				$destroy = array_key_exists($chave. '.destroy', $rules) ? 'checked' : '';
			@endphp
			<tr>
				<td>{{ $data['label'] }}</td>
				<td><input type="checkbox" name="route[]" value="{{ $chave }}.index" {{ $index }}></td>
				<td><input type="checkbox" name="route[]"  value="{{ $chave }}.store" {{  $store }}></td>
				<td><input type="checkbox" name="route[]"  value="{{ $chave }}.update" {{ $update }}></td>
				<td><input type="checkbox" name="route[]"  value="{{ $chave }}.destroy" {{ $destroy }}></td>
			</tr>
		@endforeach
	</tbody>
</table>