@extends('layouts.app')
@section('title')
	Perfil do usuário
@stop

@section('description')
	Permissões para usuários no sistema
@stop

@section('action')
	Dados do perfil
@stop

@section('content')
	{{-- Observa se a requisicao passa um model como parametro --}}
	@php
		// Caso não haja chama o metodo store para criar um registro
		$action = [
			'route' => 'profile.store'
		];
		// Caso haja, muda a rota e chama o metodo update passando como parametro o id do registro a ser editado
		if (isset($model))
			$action = [
				// rota a ser chamada
				'route' => ['profile.update', $model->id],
				// invoca o metodo PUT 
				'method' => 'PUT'
			];

	@endphp

	{{ Form::open($action) }}
		{!! csrf_field() !!}
		{{-- caso exista o model, prepara o formulario com os dados do registro passado pelo id --}}
		@if (isset($model))
			@php
				Form::setModel($model);
			@endphp
		@endif

	<div class="form-group">
		{{ Form::label('name', 'Nome do perfil') }}
		{{ Form::text('name', null, ['class'=>'form-control']) }}
	</div>

	@include('profile.acl')

	<div class="form-group">
		<a href="{{ route('profile.index') }}" class="btn btn-default">
			<i class="fa fa-arrow-left"></i>
			Voltar
		</a>
		{{ Form::submit('Salvar', ['class'=>'btn btn-success btn-save']) }}
	</div>

	{{ Form::close() }}
@stop

@section('footer')
	{{--  --}}
@stop