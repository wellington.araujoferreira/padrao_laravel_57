<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /*
    	@description nome da tabela do banco 
     */
    protected $table = 'profiles';
    /*
    	campos da tabela
     */
    protected $fillable = [
    	'name'
    ];

    public function rules(){
        return $this->hasMany(Rule::class);
    }

    public function delete(){
        foreach ($this->rules as $rule) $rule->delete();
        return parent::delete();
    }
}
