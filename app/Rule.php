<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    /*
    	@description nome da tabela do banco 
     */
    protected $table = 'rules';
    /*
    	campos da tabela
     */
    protected $fillable = [
    	'route',
    	'profile_id'
    ];

}
