<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends Model
{
    use SoftDeletes;
    /*
    	@description nome da tabela do banco 
     */
    protected $table = 'areas';
    /*
    	campos da tabela
     */
    protected $fillable = [
    	'name',
    	'description',
        'color',
        'ordem'
    ];

    public function menus(){
        return $this->hasMany(Menu::class, 'area_id', 'id');
    }
}
