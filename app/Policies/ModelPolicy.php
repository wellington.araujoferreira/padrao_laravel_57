<?php

namespace App\Policies;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Access\HandlesAuthorization;
use Route;

class ModelPolicy
{
    use HandlesAuthorization;

    public function before($user){
        if($user->admin){
            return true;
        }
    }

    /**
     * Determine whether the user can view the Model.
     *
     * @param  \App\User  $user
     * @param  \App\Model  $Model
     * @return mixed
     */
    public function acl(User $user)
    {

        //pega a url passada no browser
        $url = explode('/', $_SERVER['REQUEST_URI']);
        //pega o nome da rota
        $key = Route::currentRouteName();

        //Caso a currentRouteName = null, pega da url o nome da classe e concatena com .index"
        $key = !is_null($key) ? explode('.', $key) : $url[1] . '.index';

        if (is_array($key)) {
            switch ($key[1]) {
                case 'edit':
                    if($user->hasRules($key[0] . '.update')) return true;
                    break;
                case 'destroy':
                    if($user->hasRules($key[0] . '.destroy')) return true;
                    break;
                case 'create':
                    if($user->hasRules($key[0] . '.store')) return true;
                    break;
                case 'show':
                    if($user->hasRules($key[0] . '.index')) return true;
                    break;
                case 'new':
                    if($user->hasRules($key[0] . '.index')) return true;
                    break;
                default:
                    if($user->hasRules($key[0] . '.index')) return true;
                    break;
            }
        }

        if($user->hasRules($key)) return true;
    }

    public function index(User $user){
        return $this->acl($user);
    }

}
