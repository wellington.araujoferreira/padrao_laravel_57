<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;
    /*
        @description nome da tabela do banco 
     */
    protected $table = 'menus';
    /*
        campos da tabela
     */
    protected $fillable = [
        'name',
        'description',
        'icon',
        'area_id'
    ];

    public function submenus(){
        return $this->hasMany(Submenu::class, 'menu_id', 'id');
    }

    public function area(){
        return $this->hasOne(Area::class, 'id', 'area_id');
    }

}
