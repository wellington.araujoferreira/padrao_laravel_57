<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogInsert extends Model
{
	protected $table = 'log_inserts';
    protected $connection = 'siapenlog';
    protected $fillable = [
    	'user',
    	'remote_addr',
    	'http_user_agent',
    	'table',
    	'registro',
    	'nome',
    	'alcunha',
    	'route',
    ];
}
