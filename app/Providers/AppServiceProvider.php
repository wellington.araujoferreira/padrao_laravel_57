<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Html;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Html::component(
            'item',
            'includes.parts.bar-left.submenu',
            ['route', 'label', 'icon', 'description']
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
