<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Submenu extends Model
{
	use SoftDeletes;
	/*
	@description nome da tabela do banco 
	*/
	protected $table = 'submenus';
	/*
	campos da tabela
	*/
	protected $fillable = [
		'name',
		'description',
		'icon',
		'action',
		'route',
		'menu_id'
	];

	public function menu(){
        return $this->hasOne(Menu::class, 'id', 'menu_id');
    }	
}
