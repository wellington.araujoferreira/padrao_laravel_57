<?php  
namespace App\Classes;
use App\LocalAtendimento;
use App\AtendimentoTipo;
use App\Encaminhamento;


class Fields {

	protected static $nCount = 0;

	public function setFields($query){
		$model = $query->get();
		self::$nCount = $model->count();
	}

	public static function count(){
		return self::$nCount;
	}

	/**
	 * [getFieldId Faz pesquisa por um determinado valor em uma tabela e retorna o id encontrado
	 * @param  [class] $class       [Classe da entidade a ser pesquisada]
	 * @param  [string] $name_field [nome do campo a ser pesquisado]
	 * @param  [string] $value      [valor a ser pesquisado]
	 * @return [integer]            [Id do resgistro encontrado]
	 */
	public static function getFieldId($class, $name_field ,$value){
		// dd($class, $name_field, $value);
		$entity = $class::select()
                    ->where($name_field, 'like', '%' . $value . '%')->first();
        if(count($entity) > 0) return $entity->id;
	}

	public static function setEncaminhamento($id){
	    dd('Chegou!');
        $encaminhamento = Encaminhamento::find($id);
        $dados = [
            'id' => $id,
            'encaminhamento_id' => '1',
        ];

        $m = $encaminhamento::fill($dados);
        $m->update();
    }
}