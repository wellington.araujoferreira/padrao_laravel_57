<?php 

namespace App\Classes;
use App\FrequenciaSemanal;

class Datas {
	protected $value;

	public static function parse_br($data){
		if (strstr($data, '-')) {
			$value = explode('-', $data);
			return $value[2] . '/' . $value[1] . '/' . $value[0];
		}else{
			return $data;
		}
	}

	public static function parse_us($data){
		if (strstr($data, '/')) {
			$value = explode('/', $data);
			return $value[2] . '-' . $value[1] . '-' . $value[0];
		}else{
			return $data;
		}
	}

	public static function remCaracter($caracter, $date){
		$temp = explode($caracter, $date);
		$newValue = '';
		for ($i=0; $i <= count($temp); $i++) {
			$n = count($temp) - 1;
			if($i < 2){
				$newValue .= $temp[$i] . '-';
			}elseif ($i == 2){
				$newValue .= $temp[$i];
			}
		}
		return $newValue;
	}

	public static function hasExpired($value){
		// $value = Datas::parse_us($value);
		$data = date("Y-m-d",strtotime($value));

        $hoje = strtotime(date("Y-m-d"));

        return strtotime(Datas::parse_us($value)) < $hoje ? true : false;
	}

	/**
	 * Verifica um intervalo de datas se estão expiradas;
	 * @param  [type]  $value_initial [data inicial a ser verificada]
	 * @param  [type]  $value_finish  [data final a ser verificada]
	 * @return boolean                [se expirado retorna true]
	 */
	public static function hasExpiredBetween($value_initial, $value_finish, $value_padrao = null){
		//pega a data de hoje
		$hoje = is_null($value_padrao)? strtotime(date("Y-m-d")): strtotime($value_padrao);
		$inicio = strtotime($value_initial);
		$final = strtotime($value_finish);
		$hasExpired = true;

		if($hoje >= $inicio && $hoje <= $final){
			$hasExpired = false;
		}else{
			$hasExpired = true;
		}

		return $hasExpired;
	}

	public static function getYear($value){
		$ano = 0;

		if (strpos($value, '/')) {
			$temp = explode('/', $value);
			for ($i=0; $i < count($temp); $i++) { 
				if (strlen($temp[$i]) == 4) {
					$ano = $temp[$i];
				}
			}
		}else{
			$temp = explode('-', $value);
			for ($i=0; $i < count($temp); $i++) { 
				if (strlen($temp[$i]) == 4) {
					$ano = $temp[$i];
				}
			}
		}

		return $ano;
	}

	public static function hasDiaDaSemana($data, $frequencia_id){

		$semana = [
			'Domingo',
			'Segunda',
			'Terça',
			'Quarta',
			'Quinta',
			'Sexta',
			'Sábado',
		];

		$newDate = Datas::parse_us($data);

		$numero_dia = date('w', strtotime($newDate));

		$frequencia = FrequenciaSemanal::find($frequencia_id);

		$array = explode(',', $frequencia->descricao);

		for ($i=0; $i < count($array); $i++) { 
			$search[] = trim($array[$i]);
		}

		$resultado = in_array($semana[$numero_dia], $search);

		if ($resultado === true || $frequencia->id === 13) {
			return true;
		}else{
			return false;
		}
	}

	public static function getDiaSemana($data){
		$semana = [
			'Nenhum',
			'Segunda',
			'Terça',
			'Quarta',
			'Quinta',
			'Sexta',
			'Sábado',
			'Domingo',
		];

		$numero_dia = date('w', strtotime($data));
		if ($numero_dia == 0) {
			$numero_dia = 7;
		}
		$frequencia = FrequenciaSemanal::select()->where('descricao', '=', $semana[$numero_dia])->first();
		// dd($frequencia->id);
		return $frequencia->id;
	}

	public static function getDiaSemanaNome($data){
		$semana = [
			'Nenhum',
			'Segunda',
			'Terça',
			'Quarta',
			'Quinta',
			'Sexta',
			'Sábado',
			'Domingo',
		];

		$numero_dia = date('w', strtotime($data));
		if ($numero_dia == 0) {
			$numero_dia = 7;
		}
		$frequencia = FrequenciaSemanal::select()->where('descricao', 'like', '%' . $semana[$numero_dia] . '%')->first();
		// dd($frequencia->id);
		return $frequencia->descricao;
	}
}
