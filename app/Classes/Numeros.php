<?php

namespace App\Classes;

class Numeros {
	public static function moeda_para_us($value){
		$newValue = str_replace('.', '', $value);
		$newValue = str_replace(',', '.', $newValue);
		return $newValue;
	}

	public static function moeda_para_br($value){
		// $newValue = str_replace('.', '', $value);
		$newValue = str_replace('.', ',', $value);
		$newValue = number_format($value, 2, ',', '.');
		return $newValue;
	}
}