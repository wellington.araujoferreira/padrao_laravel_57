<?php  

namespace App\Classes;

class Strings {

	public static function soNumeros($value){
		$newValue = preg_replace('/[^0-9]/', '', $value);
		return $newValue;
	}

	public static function soLetras($value){
		$newValue = preg_replace('/[^a-zA-Z]/', '',  $value);
		return $newValue;
	}

	public static function nomeProprio($value){
		$newValue = strtolower($value);
		return ucwords($newValue);
	}

	public static function maiuscula($value){
		$newValue = strtoupper($value);
		return $newValue;
	}

	public static function search($text, $value){
		$padrao = '/' . $value . '/';

		if (preg_match($padrao, $text)) {
		  $hasValue = true;
		} else {
		  $hasValue = false;
		}

		return $hasValue;
	}
}