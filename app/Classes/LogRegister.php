<?php

namespace App\Classes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class LogRegister {
	protected $connection;
    protected $table;
    protected $class;

    public function __construct($connection, $class, $table){
        $this->connection = $connection;
        $this->class = $class;
        $this->table = $table;
        $this->enableQueryLog();
    }

    public function enableQueryLog(){
        // dump($this->connection, 'habilitando log');
        return \DB::connection($this->connection)->enableQueryLog();
    }

    /**
     * Registra os selects no banco de logs
     * @param  [type] $connection Nome do banco podendo ser null
     * @param  [type] $table      Nome da tabela
     * @return [type] Boolean     Falso ou Verdadeiro
     */
    public function select(){

        if (Auth::user()->admin == 1) {
            return false;
        }

        $this->connection = $connection;
        $this->table = $table;

        $query = \DB::connection($this->connection)->getQueryLog(); // pega a sql que foi criada
        foreach ($query as $key) {
            $strQuery = $key['query'];
        }

        \DB::connection($this->connection)->disableQueryLog();
        $method = 'log_selects';

        $strExecute = "INSERT INTO ". $method . " (remote_addr, http_user_agent, user_id, user, tables, query, created_at, updated_at)" ;
        $strExecute .= " values(?,?,?,?,?,?,?,?)";
        $strVal = array(
            $_SERVER['REMOTE_ADDR'],
            $this->getUserAgent(),
            Auth::user()->cpf,
            Auth::user()->name,
            $table,
            $strQuery,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'));
        return \DB::insert($strExecute, $strVal);
    }

    /**
     * [Registra os inserts no banco de logs]
     * @param  String  $connection Nome do banco a ser usasdo
     * @param  String  $table      Nome da tabela a ser afetada
     * @param  Request $request    Dados a serem inseridos
     * @return String              [description]
     */
    public function insert(){

        if (Auth::user()->admin == 1) {
            return false;
        }
        $strQuery = "";
        $query = \DB::connection($this->connection)->getQueryLog(); // pega a sql que foi criada
        foreach ($query as $key) {
            $strQuery = $key['query'];
            $values = $key['bindings'];
            $strValue = 'values(';
            for ($i=0; $i < count($values); $i++) {
                $strValue .= $values[$i] ."; ";
            }
            $strValue .= ')';
        }

        $temp = explode(')', $strQuery);
        $strQuery = $temp[0] . ") " . $strValue;

        \DB::connection($this->connection)->disableQueryLog();
        $method = 'log_inserts ';

        $strExecute = "INSERT INTO ". $method . " (remote_addr, http_user_agent, user_id, user, tables, query, created_at)";
        $strExecute .= " values(?,?,?,?,?,?,?)";

        $strVal = array(
            $_SERVER['REMOTE_ADDR'],
            $this->getUserAgent(),
            Auth::user()->cpf,
            Auth::user()->name,
            $this->table,
            $strQuery,
            date('Y-m-d H:i:s'),
            // date('Y-m-d H:i:s'),
        );

        return \DB::insert($strExecute, $strVal);
    }

    /**
     * [registerUpdate Registra as alteraçãoes de registros]
     * @param  [String]  $content [Conteúdo do registro antes da alteração]
     * @param  [Integer] $id      [Id do registro a ser alterado]
     * @return [Booleam]          [Verdadeiro ou falso]
     */
    public function update($content, $id){

        if (Auth::user()->admin == 1) {
            return false;
        }

        $strQuery = "";
        $strValue = "";
        $query = \DB::connection($this->connection)->getQueryLog(); // pega a sql que foi criada

        foreach ($query as $key) {
            $strQuery = $key['query'];
            $values = $key['bindings'];
            $strValue = 'values(';
            for ($i=0; $i < count($values); $i++) {
                $strValue .= $values[$i] ."; ";
            }
            $strValue .= ')';
        }
        $temp = explode(')', $strQuery);
        $strQuery = $temp[0] . ") " . $strValue;

        \DB::connection($this->connection)->disableQueryLog();
        $method = 'log_updates ';

        $strExecute = "INSERT INTO ". $method . " (remote_addr, http_user_agent, user_id, user, tables, register, content ,query, created_at, updated_at)";
        $strExecute .= " values(?,?,?,?,?,?,?,?,?,?)";

        $strVal = array(
            $_SERVER['REMOTE_ADDR'],
            $this->getUserAgent(),
            Auth::user()->cpf,
            Auth::user()->name,
            $this->table,
            (int) $id,
            $content,
            $strQuery,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s')
        );
        // dd($strExecute,$strVal);
        return \DB::insert($strExecute, $strVal);
    }

    public function delete($content, $id){

        if (Auth::user()->admin == 1) {
            return false;
        }

        $strQuery = "";
        $strValue = "";

        $query = \DB::connection($this->connection)->getQueryLog(); // pega a sql que foi criada

        foreach ($query as $key) {
            $strQuery = $key['query'];
            $values = $key['bindings'];
            $strValue = 'values(';
            for ($i=0; $i < count($values); $i++) {
                $strValue .= $values[$i] ."; ";
            }
            $strValue .= ')';
        }
        $temp = explode(')', $strQuery);
        $strQuery = $temp[0] . ") " . $strValue;

        \DB::connection($this->connection)->disableQueryLog();
        $method = 'log_deletes ';

        $strExecute = "INSERT INTO ". $method . " (remote_addr, http_user_agent, user_id, user, tables, register, content ,query, created_at, updated_at)";
        $strExecute .= " values(?,?,?,?,?,?,?,?,?,?)";

        $strVal = array(
            $_SERVER['REMOTE_ADDR'],
            $this->getUserAgent(),
            Auth::user()->cpf,
            Auth::user()->name,
            $this->table,
            (int) $id,
            $content,
            $strQuery,
            date('Y-m-d H:i:s'),
            date('Y-m-d H:i:s'));

        return \DB::insert($strExecute, $strVal);
    }

    /**
     * [Retorna o registro requerido e seu conteúdo antes do update ou delete]
     * @param  Class $class [Class da entidade do banco de dados]
     * @param  Integer $id  [Registr a ser pesquisado]
     * @return String       [Conteudo encontrado]
     */
    public function getRegister($id){
        $class = $this->class;

        $obj = $class::select()->where('id', '=', $id)->get();

        $content= $this->table . " (";

        $fields = $this->getField();

        foreach ($obj as $registro) {
            foreach ($fields as $key => $value) {
                $content .= $value . '=' . $registro->$value . "; ";
            }
            $content .= ")";
        }

        return $content;
    }

    /**
     * Pega os campos de uma tabela
     * @return [type] Array com o nome dos campos da tabela;
     */
    private function getField(){

        $tab = DB::connection($this->connection)->select('describe ' . $this->table);
        $fields = collect($tab)->pluck('Field');
        return $fields;
    }

    /**
     * Pega o nome do aparelho pelo qual o usuario está acessando a aplicação
     * @return String   Nome do aparelho
     */
    private function getUserAgent(){
        $http = explode(';', $_SERVER['HTTP_USER_AGENT']);
        $temp = explode(')', $http[1]);
        $equipamento = trim($temp[0]);
        return $equipamento;
    }
}