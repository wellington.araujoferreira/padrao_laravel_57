<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
// use Illuminate\Contracts\Auth\CanResetPassword;

use App\Classes\Strings;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $connection = 'siapenadm';
    protected $fillable = [
        'name',
        'email',
        'cpf',
        'password',
        'admin',
        'img',
        'type',
        'size',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function funcao(){
        return $this->hasOne(Funcao::class, 'id', 'funcao_id');
    }

    public function encaminharSetor(){
        return $this->hasOne(EncaminharSetor::class, 'id', 'encaminhar_setor_id');
    }

     /**
     * The roles that belong to the user.
     */
    public function profiles()
    {
        return $this->belongsToMany(Profile::class
            , 'user_profiles'
            , 'user_id' //campo da tabela atual
            , 'profile_id' //campo da tabela de relacionamento
        );
    }

    public function getRules(){
        $key = $this->cpf . 'auth-rules';
        //pega o perfil do usuario
        if (! session()->has($key)) {
            $rules = [];
            $perfis = $this->profiles;
            foreach ($perfis as $perfil) {
                foreach ($perfil->rules as $rule) {
                    $rules[$rule->route] = $rule->route;;
                }
            }
            session([$key => $rules]);
        }
        return session($key);
    }

    public function hasRules($route){
        // se for administrador deixa passar

        // $result = $this->updateFieldes();
        $entidade = explode('.', $route);

        // if ($entidade[0] == 'user' && $entidade[1] == 'update') {dd($route);

        if ($this->admin) return true;

        if (str_contains($route, '.edit')) {
            // dump($route);
            $route = str_replace('.edit', '.update', $route);
        }

        if (str_contains($route, '.create')) {
            $route = str_replace('.create', '.store', $route);
        }

        // caso contrario verifa as permissões
        return array_key_exists($route, $this->getRules());
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }

    public function setCpfAttribute($value){
        $this->attributes['cpf'] = Strings::soNumeros($value);
    }

    public function getCpfAttribute($value){
        return Strings::soNumeros($value);
    }

    public function getNameAttribute($value){
        return Strings::nomeProprio($value);
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = Strings::nomeProprio($value);
    }

    public function updateFieldes(){
        $class = '\App\Submenu';
        $value = 'agendaAtendimento.index';
        $new_value = ['name' => 'agendaAtendimento.index'];
        $fields = 'name';
        dump($fields, $value);
        $model = $class::select()->where($fields, '=', $value)->get();

        // dd($model);
        if (count($model)) {
            $id = $model->id;
            //efetuando a alteracao dos dados
            $model = $class::find($id)->fill($new_value);
            // \DB::connection($this->connection)->enableQueryLog();
            $logRegister->enableQueryLog();
            if ($model->update()) {
                dd('Registro atualizado com sucesso');
            }else{
                dd('Não foi possível atualizar o registro!');
            }
        }

        return true;
    }
}
