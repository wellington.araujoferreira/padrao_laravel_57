<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogUpdate extends Model
{
    protected $table = 'log_updates';
    protected $connection = 'siapenlog';
    protected $fillable = [
    	'user',
    	'remote_addr',
    	'http_user_agent',
    	'table',
    	'registro',
    	'nome',
    	'alcunha',
    	'route',
    ];
}
