<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogSelect extends Model
{
    protected $table = 'log_selects';
    protected $connection = 'siapenlog';
    protected $fillable = [
    	'user',
    	'remote_addr',
    	'http_user_agent',
    	'table',
    	'registro',
    	'nome',
    	'alcunha',
    	'route',
    ];
}
