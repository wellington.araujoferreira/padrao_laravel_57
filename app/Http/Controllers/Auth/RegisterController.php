<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'         => 'required|string|max:255',
            'email'        => 'required|string|email|max:255|unique:users',
            'password'     => 'required|string|min:6|confirmed',
            'cpf'          => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $user = User::create($data);
        if (isset($data['profile_id']))
            $user->profiles()->sync($data['profile_id']);

        session()->flash('success', 'Registro salvo com sucesso!');
        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $file = $request->file('imagem');

        // $cpf = str_replace(".","", $request->input('cpf'));
        // $cpf = str_replace("-","", $cpf);

        $dados = [
            'name'         => $request->input('name'),
            'email'        => $request->input('email'),
            'password'     => $request->input('password'),
            'funcao_id'    => $request->input('funcao_id'),
            'num_conselho' => $request->input('num_conselho'),
            'cpf'          => $request->input('cpf'),
            'profile_id'   => $request['profile_id'],
        ];

        if ($file) {
            $img = base64_encode(file_get_contents($file->getRealPath()));
            $type = $file->getMimeType();
            $size = $file->getSize();
            $dados = [
                'name'         => $request->input('name'),
                'email'        => $request->input('email'),
                'password'     => $request->input('password'),
                'funcao_id'    => $request->input('funcao_id'),
                'num_conselho' => $request->input('num_conselho'),
                'profile_id'   => $request['profile_id'],
                'cpf'          => $request->input('cpf'),
                'img'          => $img,
                'type'         => $type,
            ];
        }

        // $user = User::create($dados);

        event(new Registered($user = $this->create($dados)));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }
}
