<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
use Route;
use App\Classes\LogRegister;
use App\Classes\Fields;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{
    protected $class = '\App\User';
    protected $path = 'user';
    protected $table = 'users';
    protected static $validate = [
        'name'=>'required',
        'email'=> 'required',
        // 'password'=>'required',
        // 'admin'=>'required',
        'cpf'=>'required'
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->checkPassword()) return redirect()->route('user.show', Auth::user()->cpf);

        $class = $this->class;

        $this->authorize('acl', $this->class);

        $query = $class::select();

        if (request()) {
            // captura o id passado pelo request e inclui na consulta
            if (!is_null(request('id'))) $query->where('id', 'like', '%'.request('id').'%');

            // Capitura o nome passado pelo request e inclui na consulta
            if (!is_null(request('name'))) $query->where('name', 'like', '%'.request('name').'%');

            //  Capitura o cpf passado pelo request e inclui na consulta
            if (!is_null(request('cpf'))) $query->where('cpf', 'like', '%'.request('cpf').'%');

            //  Capitura o cpf passado pelo request e inclui na consulta
            if (!is_null(request('email'))) $query->where('email', 'like', '%'.request('email').'%');
        }

        return view('user.index')
            ->with('id', request('id'))
            ->with('name', request('name'))
            ->with('cpf', request('cpf'))
            ->with('email', request('email'))
            ->with('models', $query->paginate(10));
    }

	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dump('antes');
    	$class = $this->class;
        $user = $class::find($id);
       	$result = '';
        $newPassword = $request->input('newPassword');
        if(isset($newPassword)){
            $result = $this->forgotPassword($request, $id);
            $routeTrue = 'home';
            $route = $this->path . '.show';
            $param = $user->cpf;
        }else{
            $this->authorize('acl', $this->class);
            $result = $this->save($request, $id);
            $routeTrue = $this->path . '.index';
            $routeFalse = $this->path . '.edit';
            $param = $id;
        }

        if ($result['result']){
            return redirect()->route($routeTrue)->with('success', $result['msg']);
        }else{
            return redirect()->route($routeFalse, $param)->with('warning', $result['msg']);
        }
    }

    public function show($id){
        $userQuery = User::select();
        $user = $userQuery->where('cpf', '=', $id)->get();
        $class = $this->class;

        foreach ($user as $u) {
            $id = $u->id;
        }

        return view($this->path . '.password')->with('model', $class::find($id));
    }

    public function save(Request $request, $id){
        $class = $this->class;
        // dd($this->class);

        //se houver itens pra validar, valida
        if(count(static::$validate)) $this->validate($request, static::$validate);

        $data = $this->resetPassword($request);

        $model = $class::find($id)->fill($data);

        $user = $class::find($model->id);
        //habilita o registro de log da operacao
        $logRegister = new LogRegister($this->connection, $this->class, 'users');
        //pega o conteudo do registro antes de altera-lo

        $antes = $logRegister->getRegister($id);
        if ($model->update()){
            //Registra em log a operacao
            $logRegister->update($antes, $id);
            if (isset($data['profile_id'])) $user->profiles()->sync($data['profile_id']);
            $return = ['result'=>true, 'msg'=>'Registro salvo com sucesso!'];
        }else{
            $return = ['result'=>false, 'route'=>$this->path . '.edit'];
        }

        return $return;
    }

    public function forgotPassword(Request $request, $id){
        $class = $this->class;
        $user = $class::find($id);
        $cpf = $user->cpf;
        $return = ['result'=>false, 'msg'=>'Algo deu errado!'];
        if(Hash::check($request->input('passOld'), $user->password)){
            if($request->input('newPassword') == $request->input('password')){
                $data = [
                    // 'id' => $id,
                    'password' => $request->input('password'),
                ];
                $model = $class::find($id)->fill($data);
                //habilita o registro de log da operacao
                $logRegister = new LogRegister($this->connection, $this->class, 'users');
                //pega o conteudo do registro antes de altera-lo
                $antes = $logRegister->getRegister($id);
                if($model->update()){
                    //Registra em log a operacao
                    $logRegister->update($antes, $id);
                    $return = ['result'=>true, 'msg'=>'Senha atualizada com sucesso!'];
                }else{
                    $return = ['result'=>false, 'msg'=>'Não foi possível mudar a senha!'];
                }
            }else{
                $return = ['result'=>false, 'msg'=>'As senhas são diferentes!'];
            }
        }

        return $return;
    }

    public function resetPassword(Request $request){
        $chaves = array(); $valores = array(); $i = 0; $nt=0; $total=0;
        foreach ($request as $key => $value) {
            if ($key == 'request') {
                $itens = $value;
                foreach ($itens as $k => $v) {
                    $total = (count($itens) - 2) - $nt;
                    if($i > 1){
                        if (!is_null($v)) {
                            $chaves[] = $k;
                            $valores[] = $v;
                        }
                    }
                    $i++;
                }
            }
        }

        if($request->file('imagem')){
            $file = $request->file('imagem');
            if ($file) {
                $img = base64_encode(file_get_contents($file->getRealPath()));
                $type = $file->getMimeType();
                $size = $file->getSize();
                $chaves[] = 'img';
                $valores[] = $img;
                $chaves[] = 'type';
                $valores[] = $type;
            }
        }

        $dados = array_combine($chaves, $valores);
        return $dados;
    }

}
