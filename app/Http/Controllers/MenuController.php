<?php

namespace App\Http\Controllers;

use App\Menu;
use Illuminate\Http\Request;

class MenuController extends BaseController
{
    protected $class = '\App\Menu';
    protected $table = 'menus';
    // protected $connection = 'siapenadm';
    protected $path = 'menu';
    protected static $validate = [
        'name'=>'required|min:5|',
        'description'=>'required:10',
        'icon'=>'required'
    ];
}
