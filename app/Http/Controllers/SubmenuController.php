<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Illuminate\Support\Facades\Auth;

class SubmenuController extends BaseController
{
    protected $class = '\App\Submenu';
    protected $path = 'submenu';
    protected $table = 'submenus';
    // protected $connection = 'siapenadm';
    protected static $validate = [
        'name'=>'required|min:5|',
        'description'=> 'required|min:15',
        'icon'=> 'required:5',
        'action'=> 'required:10',
        'route'=> 'required:10',
        'menu_id'=>'required',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->checkPassword()) return redirect()->route('user.show', Auth::user()->cpf);

        $class = $this->class;

        $this->authorize('acl', $this->class);

        $query = $class::select();

        // dd(request());

        if (request()) {
            // captura o id passado pelo request e inclui na consulta
            if (request()->has('id')) $query->where('id', 'like', '%'.request('id').'%');

            // Capitura o title passado pelo request e inclui na consulta
            if (!is_null(request('menu'))) $query->where('menu_id', '=', $this->getIdMenu(request('menu')));

            // Capitura o title passado pelo request e inclui na consulta
            if (!is_null(request('submenu'))) $query->where('name', 'like' ,'%' . request('submenu') .'%');

            //  Capitura o alcunha passado pelo request e inclui na consulta
            if (!is_null(request('description'))) $query->where('description', 'like', '%'.request('description').'%');
        }

        return view('submenu.index')
            ->with('id', request('id'))
            ->with('menu', request('menu'))
            ->with('submenu', request('submenu'))
            ->with('description', request('description'))
            ->with('models', $query->paginate(10));
    }

    public function getIdMenu($value){
        $status = Menu::select()->where('name', 'like', '%$valor%')->first();
        // dd($status);
        if(count($status) > 0) return $status->id;
    }

    public function getIdSubMenu($value){
        $status = SubMenu::select()->where('name', 'like', '%$valor%')->first();
        // dd($status);
        if(count($status) > 0) return $status->id;
    }
}
