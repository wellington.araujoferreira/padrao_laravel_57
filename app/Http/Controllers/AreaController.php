<?php

namespace App\Http\Controllers;

use App\Area;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AreaController extends BaseController
{
    protected $class = '\App\Area';
    protected $path = 'area';
    protected $table = 'areas';
    // protected $connection = 'siapenadm';
    protected static $validate = [
        'name'=>'required|min:5|',
        'description'=>'required:10',
        'color'=>'required',
        'ordem'=>'required'
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       	$class = $this->class;
       	$this->authorize('index', $this->class);
        if($this->checkPassword()) return redirect()->route('user.show', Auth::user()->cpf);
        // lista as categorias
        return view($this->path.'.index')->with(
            'models', $class::orderBy('ordem')->paginate(10)
        );
    }
}
