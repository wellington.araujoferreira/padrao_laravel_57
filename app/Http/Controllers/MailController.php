<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class MailController extends BaseController
{

    protected $path = 'mail';
    protected $class = '\App\Mail';
    // protected $connection = 'siapenadm';
    protected static $validate = [
        'usuario'=>'required',
        'email'=>'required',
        'mensagem'=>'required',
    ];


        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->checkPassword()) return redirect()->route('user.show', Auth::user()->cpf);

       	$class = $this->class;

       	$this->authorize('acl', $this->class);

        return view($this->path.'.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$class = $this->class;
       	$this->authorize('acl', $class);

        // dd($request);

        //se houver itens pra validar, valida
        if(count(static::$validate)) $this->validate($request, static::$validate);

        // Enviar email


        if(Mail::send('mail.send', $request->all(), function($msg){
        	$msg->subject('Envio de correio eletrônico');
        	$msg->to('wellington.araujoferreira@gmail.com');})
    	){
            return redirect()->route($this->path.'.index')->with('success', 'E-mail enviado com sucesso!');
        }else{
            return redirect()->route($this->path.'.index')->with('warning', 'Não foi possível enviar o e-mail!');
        }
    }
}
