<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Classes\LogRegister;

class BaseController extends Controller
{
	protected static $validate;
    protected $class;
	protected $path;
    protected $connection;
    protected $table;

    function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if($this->checkPassword()) return redirect()->route('user.show', Auth::user()->cpf);

       	$class = $this->class;
        $k = ""; $v="";
       	$this->authorize('acl', $this->class);

        return view($this->path.'.index')->with(
            'models', $class::orderBy('id', 'desc')->paginate(10)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$class = $this->class;
       	$this->authorize('acl', $this->class);
        return view($this->path . '.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$class = $this->class;
       	$this->authorize('acl', $this->class);

        // dd($request);

        //se houver itens pra validar, valida
        if(count(static::$validate)) $this->validate($request, static::$validate);

        // Habilita o registro de log de insert
        $logRegister = new LogRegister($this->connection, $this->class , $this->table);
        $logRegister->enableQueryLog();
        //inseri os dados e registra no log
        if($class::create($request->all())){
            $result = $logRegister->insert();
            return redirect()->route($this->path.'.index')->with('success', 'Registro salvo com sucesso!');
        }else{
            return redirect()->route($this->path.'.create');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	$class = $this->class;
       	// $this->authorize('update', $this->class);

        return view($this->path.'.create')->with('model', $class::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$class = $this->class;
       	$this->authorize('acl', $this->class);

        //se houver itens pra validar, valida
        if(count(static::$validate)) $this->validate($request, static::$validate);

        //preparar para registrar o log do evento
        $logRegister = new LogRegister($this->connection, $this->class, $this->table);
        $antes = $logRegister->getRegister($id);

        //efetuando a alteracao dos dados
        $model = $class::find($id)->fill($request->all());
        // \DB::connection($this->connection)->enableQueryLog();
        $logRegister->enableQueryLog();
        if ($model->update()) {
            //registrando o log da operacao
            $logRegister->update($antes, $id);
            return redirect()->route($this->path.'.index')->with('success', 'Registro salvo com sucesso!');
        }else{
            redirect()->route($this->path.'.edit', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	$class = $this->class;
       	$this->authorize('acl', $this->class);

        //preparar para registrar o log do evento
        $logRegister = new LogRegister($this->connection, $this->class, $this->table);
        $antes = $logRegister->getRegister($id);

        //Registra a deleção do registro
        $logRegister->enableQueryLog();
        if ($class::destroy($id)) {
            //registrando o log da operacao
            $logRegister->delete($antes, $id);
            session()->flash('success', 'Excluído com sucesso!');
        }else{
            session()->flash('danger', 'Falha ao excluir o ítem!');
        }

        return redirect()->route($this->path.'.index');
    }

    /**
     * [checkPassword Verifica se o usuario trocou a senha padrão por uma senha personalizada]
     * @return [type] [description]
     */
    public function checkPassword(){
        $user = Auth::user();
        // dd(Hash::check('123456', $user->password));
        //checa a senha
        return  Hash::check('123456', $user->password)
        ? true
        : false;
    }

    public function updateIcons(){
        if ($this->class == '\App\Submenu') {
            $class = '\App\Submenu';
            $submenus = $class::select()->orderBy('id')->get();
            foreach ($submenus as $submenu) {
                $dados = ['icon' => 'fa ' .$submenu->icon ];

                $model = $class::find($submenu->id)->fill($dados);

                if($model->update()){
                    dump($submenu->id . ' icon: ' . $submenu->icon . ' Atualizado com sucesso!');
                }else{
                    dump($submenu->id . ' icon: ' . $submenu->icon . ' Não foi possível atualizar o icone!');
                }
            }
        }
    }
}
