<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use DB;
use App\Rule;
// use Auth;
use App\Classes\LogRegister;
use Illuminate\Support\Facades\Auth;

class ProfileController extends BaseController
{
    protected $class = '\App\Profile';
    protected $path = 'profile';
    protected static $validate = [
        'name'=>'required|min:3|',
    ];


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //policy
        $this->authorize('index', $this->class);
        if($this->checkPassword()) return redirect()->route('user.show', Auth::user()->cpf);

        return view('profile.index')->with(
            'models', Profile::orderBy('id', 'desc')->paginate(10)
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $this->authorize('index', $this->class);
        return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //policy
        // $this->authorize('index', $this->class);

        $this->validate($request, [
            'name' => 'required|unique:profiles|max:30|min:3',
        ]);

        if($request->has('route')){

            DB::beginTransaction();
            //pega os rules passados pelo request
            foreach ($request->get('route') as $route)
                $rules[] = new Rule(['route'=>$route]);

            //preparar para registrar o log do evento
            $logRegister = new LogRegister($this->connection, $this->class, $this->table);
            $profile = Profile::create($request->all());
            $logRegister->insert();
            if ($profile->rules()->saveMany($rules)){
                DB::commit();
                return redirect()
                        -> route('profile.index')
                        -> with('success', "Salvo com sucesso!");
            }
            DB::rollback();
        }else{
            session()->flash('info', 'Selecione pelo menos uma ação para uma rota!');
        }

        return redirect()->route('profile.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //policy
        // $this->authorize('index', $this->class);
        return view('profile.create')->with('model', Profile::find($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //policy
        // $this->authorize('index', $this->class);

        $this->validate($request, [
            'name' => 'required|max:50|min:3',
        ]);

        DB::beginTransaction();

        $model = Profile::find($id);
        //dd($request->get('route'));
        $rules = $model->rules()->whereNotIn(
            'route', $request->get('route')
        )->get();

        // Apagar as alterações existentes
        if($rules->count()){
            foreach ($rules as $rule) {$rule->delete();}
            $rules = null;
        }

        // Adicionando as diferenças ou seja o que há de novo
        foreach ($request->get('route') as $route) {
            if (!$model->rules()->where('route', $route)->count()) {
                $rules [] = new Rule(['route'=>$route]);
            }
        }

        // se ha mudanças ele salva
        if(!is_null($rules)) $model->rules()->saveMany($rules);

        $model->fill($request->all());
        if ($model->update()) {
            DB::commit();
            return redirect()
                ->route('profile.index')
                ->with('success', 'Atualizado com sucesso!');
        }else{
            dd('Não alterou nada');
        }

        DB::rollback();
        session()->flash('info', 'Nada foi alterado!');

        return redirect()->route('profile.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //policy
        // $this->authorize('destroy', $this->class);
        if (Profile::find($id)->delete() )  //nao deleta, coloca uma flag 'deletado'
            session()->flash('success', 'Excluído com sucesso!');
        else
            session()->flash('danger', 'Falha ao excluir o ítem!');

        return redirect()->route('profile.index');
    }
}
