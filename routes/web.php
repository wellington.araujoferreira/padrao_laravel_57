
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/app', function(){
	return view('adminlte3');
});

Route::get('/adminlte3', function(){
	return view('adminlte3');
});s

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::resource('profile', 'ProfileController');
Route::resource('user', 'UserController');
Route::resource('area', 'AreaController');
Route::resource('menu', 'MenuController');
Route::resource('submenu', 'SubmenuController');
// Rotas de atividades do sistema
Route::resource('mail', 'MailController');


Route::get('/avaliacaoSocial/pessoa/{id}', 'AvaliacaoSocialController@pessoa');

// Exemplos de rotas
Route::get('/api/post', function () {
    return response()->json(Post::paginate(20));
});
