#efetua a configuração inicial do sistema
# composer update
echo "Concede permissão na pasta storage e bootstrap"
chmod -R 777 storage
sudo chmod 777 storage/logs/laravel.log
chmod 777 bootstrap/autoload.php
chmod -R 777 bootstrap/cache

echo "..."
sleep 5

#Serve para editar o arquivo AuthenticateUsers.
#Troca o login de EMAIL para CPF
echo "Abre o arquivo AuthenticatesUsers para editar, trocar email para cpf"
echo "Abrindo arquivo para edição..."
sleep 5
nano vendor/laravel/framework/src/Illuminate/Foundation/Auth/AuthenticatesUsers.php
