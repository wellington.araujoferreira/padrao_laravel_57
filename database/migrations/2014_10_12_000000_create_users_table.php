<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('cpf', 11)->unique();
            $table->string('password');
            $table->integer('funcao_id')->unsigned();
            $table->string('num_conselho', 20)->nullable();
            $table->integer('encaminhar_setor_id')->unsigned()->default(0);
            $table->boolean('admin')->default(false);
            $table->longText('img')->nullable();
            $table->string('type', 30)->nullable();
            $table->string('size', 20)->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
