<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogInsertsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_inserts', function (Blueprint $table) {
            $table->increments('id');
            $table->char('remote_addr', 20);
            $table->char('http_user_agent', 30);
            $table->char('user_id', 11)->nullable();
            $table->char('user', 60)->nullable();
            $table->char('tables', 80)->nullable();
            $table->text('query')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_inserts');
    }
}
