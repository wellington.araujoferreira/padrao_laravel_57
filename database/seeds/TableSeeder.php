<?php

use Illuminate\Database\Seeder;

class TableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create();
        $this->call(AreaTableSeeder::class);
        $this->call(MenuTableSeeder::class);
        $this->call(SubmenuTableSeeder::class);
    }
}