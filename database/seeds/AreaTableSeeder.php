<?php

use Illuminate\Database\Seeder;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = $this->getAreas();
        foreach ($areas as $area) {
        	factory(App\Area::class)->create($area);
        }
    }

    /**
     * [getAreas description]
     * @return [type] [description]
     */
	private function getAreas(){
    	$areas = [
			[
				'name'=>'GERAL',
				'description'=>'Registro geral do sistema',
				'color'=>'text-default', 'ordem'=>'1'
			],
			[
				'name'=>'CONFIGURAÇÕES',
				'description'=>'Gerenciar dados do sistema',
				'color'=>'text-aqua',
				'ordem'=>'3'
			],
		];
		return $areas;
    }
}
