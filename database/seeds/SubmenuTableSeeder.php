<?php

use Illuminate\Database\Seeder;

class SubmenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Schema::hasTable('submenus')) {
            Schema::create('submenus', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name', 40);
                $table->string('description', 70);
                $table->string('icon', 40);
                $table->string('action', 100);
                $table->string('route', 60);
                $table->integer('menu_id')->unsigned();
                $table->foreign('menu_id')->references('id')->on('menus');
                $table->softDeletes();
                $table->timestamps();
            });
        } else {
            DB::table('submenus')->truncate();
            $submenus = $this->getSubmenus();
            foreach ($submenus as $submenu) {
                factory(App\Submenu::class)->create($submenu);
            }
        }
    }

    private function getSubmenus()
    {
        $submenus = [
            [
                'name' => 'Áreas',
                'description' => 'Cadastrar áreas do sistema',
                'icon' => 'fa-arrow-circle-down',
                'action' => 'Lista de  áreas cadastradas',
                'route' => 'area.index',
                'menu_id' => 2,
            ],
            [
                'name' => 'Menus do sistema',
                'description' => 'Cadastrar menus do sistema',
                'icon' => 'fa-list',
                'action' => 'Lista de menus cadastrados',
                'route' => 'menu.index',
                'menu_id' => 2,
            ],
            [
                'name' => 'Submenus do sistema',
                'description' => 'Cadastrar Submenus do sistema',
                'icon' => 'fa-list-ol',
                'action' => 'Lista de Submenus cadastrados',
                'route' => 'submenu.index',
                'menu_id' => 2,
            ],
            [
                'name' => 'Usuários do Sistema',
                'description' => 'Cadastrar usuários do sistema',
                'icon' => 'fa-users',
                'action' => 'Lista de usuários cadastrados',
                'route' => 'user.index',
                'menu_id' => 3,
            ],
            [
                'name' => 'Perfís de usuários',
                'description' => 'Cadastrar permissões para usuários dentro do sistema',
                'icon' => 'fa-cog',
                'action' => 'Lista de perfís de usuários cadastrados',
                'route' => 'profile.index',
                'menu_id' => 3,
            ],
            [
                'name' => 'Mala direta',
                'description' => 'Enviar e-mail',
                'icon' => 'fa-mail',
                'action' => 'Mala Direta',
                'route' => 'mail.index',
                'menu_id' => 1,
            ],
        ];

        return $submenus;
    }
}
