<?php

use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = $this->getMenus();
        foreach ($menus as $menu) {
        	factory(App\Menu::class)->create($menu);
        }
    }

    /**
     * [getMenus description]
     * @return [type] [description]
     */
	private function getMenus(){
    	$menus = [
	    	[
		        'name' => 'Tabelas do sistema',
		        'description' => 'Cadastrar e alterar configurações do sistema',
		        'icon' => 'fa-database',
		        'area_id' => 2
		    ],
		    [
		        'name' => 'Áreas do sistema',
		        'description' => 'Cadastrar e alterar áreas, menus e submenus do sistema',
		        'icon' => 'fa-arrow-circle-down',
		        'area_id' => 2
		    ],
		    [
		        'name' => 'Administração',
		        'description' => 'Cadastrar e alterar usuários e seus perfis',
		        'icon' => 'fa-briefcase',
		        'area_id' => 2
		    ],
		    [
		        'name' => 'Pessoa',
		        'description' => 'Serviços oferecidos a pessoa usuária da DEFENAP',
		        'icon' => 'fas fa-male',
		        'area_id' => 1
		    ],

		];
		return $menus;
    }
}
