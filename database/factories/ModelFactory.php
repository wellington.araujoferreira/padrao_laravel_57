<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => 'wellington de araujo ferreira',
        'email' => 'wellington@iapen.ap.gov.br',
        'cpf' => '32517394253',
        'funcao_id' => 2,
        'num_conselho' => 0,
        'encaminhar_setor_id' => 2,
        'password' => 'wellington',
        'admin' => true,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Area::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description'=>$faker->sentence,
        'color'=>'',
        'ordem'=>''
    ];
});

$factory->define(App\Menu::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description'=>$faker->sentence,
        'icon'=>'',
        'area_id'=>''
    ];
});

$factory->define(App\Submenu::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
        'description'=>$faker->sentence,
        'icon'=>'',
        'action'=>'',
        'route'=>'',
        'menu_id'=>''
    ];
});