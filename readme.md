<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

<div class="box box-solid box-info">
	<h1>
		## Sobre o projeto SIAPEN
	</h1>
</div>


SIAPEN é uma aplicação web para administrar dados, atividades e tratamento penal de custodiados da justiça que cumprem pena no INSTITUTO DE ADMINISTRAÇÃO PENITENCIÁRIA DO ESTADO DO AMAPA - IAPEN.

O sistema SIAPEN dispõe dos seguintes módulos:
<p>
1- Custodiados;<br>
	1.1- Cadastro;<br>
	1.2- Vinculos parentais e afetivos;<br>
	1.3- Registro criminal;<br>
	1.4- Registro de ocorrencias internas;<br>
	1.5- Movimentação do custodiado (escoltas para audiencias, hospitais e postos de saúde);<br>
	1.6- Registro de movimentação internav (modança de alojamento, mudança de prédio por motivo de progressão ou regressão de pena)<br>
</p>
<p>
2- Tratamento penal;<br>
	2.1- Assistencia Social;<br>
		2.1.1- Registro de assistencia ao custodiado;<br>
		2.1.2- Agendamento de atendimento ao custodiado;<br>
	2.2- Assistencia médica;<br>
		2.2.1- Agendamento de consulta médica ao custodiado;<br>
		2.2.2- Registro de atendimento médico ao custodiado;<br>
	2.3- Assistencia odontológica;<br>
		2.3.1- Agendamento de consulta odontológica ao custodiado;<br>
		2.3.2- Registro de atendimento odontológica ao custodiado;<br>
	2.4- Assistencia psicossocial;<br>
		2.4.1- Agendamento de consulta psicolocogica ao custodiado;<br>
		2.4.2- Registro do atendimento psicologico ao custodiado;<br>
	2.5- Assistencia farmaceutica;<br>
		2.5.1- Registro de distribuição de medicamentos ao custodiado mediante receita;<br>
	2.6- Assistencia Jurídica;<br>
		2.6.1- Agendamento de atendimento jurídico ao custodiado;<br>
		2.6.2- Registro do atendimento jurídico ao custodiado;<br>
	2.7- Atividades Laborais;<br>
		2.7.1- Inclusão do custodiado projetos laborais;<br>
		2.7.2- Cadastro de empresas e ou pessoas físicas promotoras de trabalhos laborais;<br>
		2.7.3- Registro de ocorrencias no ambiente de trabalho do custodiado;<br>
	2.8- Atividade Educacionais;<br>
		2.8.1- Inclusão do custodiados em atividades educacionais;<br>
		2.8.2- Registro de ocorrencias no ambiente escolar - educacional;<br>
	2.9- Atvidades Religiosas;<br>
		2.9.1- Inclusão do custodiados em atividades religiosas;<br>
		2.9.2- Registro de entidades religiosas e responsaveis em promover reuniões de cultos;<br>
		2.9.3- Agendamento de reunião de culto;<br>
	2.10- Estatística prisional;<br>
		2.10.1- Relatorios periodicos sobre os custodiados e seu tratamento penal;<br>
</p>
<p>
3- Administração Penitenciária;<br>
	3.1- Registro dos servidores que terão acesso ao sistema SIAPEN;<br>
	3.2- Registro de permissões e perfis dos usuários do sistema;<br>
	3.3- Cadastro de áres, menus e submenus do sistema SIAPEN;<br>
	3.4- Criação de relatórios do sistema;<br>
	3.5- Auditoria dos dados do sistema;<br>
</p>

## Desenvolvedores:

O Sistema SIAPEN está sendo desenvolvido pelos servidores efetivos do IAPEN abaixo listados>

* WELLINGTON DE ARAUJO FERREIRA - Agente Penitenciário / Gerente de Informática do IAPEN - Desenvolvedore de Sistemas (JAVA e PHP /ORACLE, PostGrees, MySQL);

* BENAIL JAMIN COSTA GIRÃO - Agente Penitenciário / Subgerente de Informática do IAPEN
<br>
* KLAUS MEDEIROS NASCIMENTO - Subgerente de informática do IAPEN

## INICIO DO PROJETO
Data: maio/2016