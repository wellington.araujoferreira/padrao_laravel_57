$(document).ready(function () {
    
    $('#formUsuario').validate({
        rules: {
            cpf: {
                required: true,
            },
            nome: { 
                //required: true,
                minlength: 15,
            },
            email: {
                //required: true,
                email: true,
            },
            senhaAtual: {
                required: true,
                minlength: 8,
                maxlength: 11,
            },
            senha1: {
                required: true,
                minlength: 8,
                maxlength: 11,
            },
            senha2: {
                required: true,
                minlength: 8,
                maxlength: 11,
            },
        },
        
        messages: {
            cpf: {
                required: "Campo de preenchimento obrigatório",
            },
            nome: {
                required: "Campo de preenchimento obrigatório",
                minlength: "Este campo deve ter no mínimo 15 dígitos!",
            },
            email: {
                required: "Campo de preenchimento obrigatório",
                email: "Informe um email válido!",
            },
            senha: {
                required: "Campo de preenchimento obrigatório",
                minlength: "Este campo deve ter no mínimo 10 dígitos!",
            },
            senhaAtual: {
                required: "Campo de preenchimento obrigatório",
                minlength: "Este campo deve ter no mínimo 8 dígitos!",
                maxlength: "Este campo deve ter no máximo 11 dígitos!",
            },
            senha1: {
                required: "Campo de preenchimento obrigatório",
                minlength: "Este campo deve ter no mínimo 8 dígitos!",
                maxlength: "Este campo deve ter no máximo 11 dígitos!",
            },
            senha2: {
                required: "Campo de preenchimento obrigatório",
                minlength: "Este campo deve ter no mínimo 8 dígitos!",
                maxlength: "Este campo deve ter no máximo 11 dígitos!",
            },
        }
    });

    $('#data').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR"
    });

    /**
     * [Masked Mascara de campos]
     * @type {String}
     */
    $('#cnpj').mask('99.999.999/9999-99', {placeholder: "#"});
    $('#cpf').mask('999.999.999-99', {placeholder: "#"});
    $('#cep').mask('99999-999', {placeholder: "#"});
    $('#fone').mask('(99)99999-9999', {placeholder: "#"});
    $('#fixo').mask('(99)9999-9999', {placeholder: "#"});
    $('#celular').mask('(99)99999-9999', {placeholder: "#"});
    $('#contato').mask('(99)99999-9999', {placeholder: "#"});

});

/**
 *  CAMPO DO TIPO DATA PADRAO PT-BR
 */