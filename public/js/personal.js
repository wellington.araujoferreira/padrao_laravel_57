$(document).ready(function(){
	$("#alerta").click(function(){
		swal({
			title: "Você tem certeza?",
			text: "Você não será capaz de recuperar esse arquivo imaginário!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Sim, excluí-lo!",
			closeOnConfirm: false
		},
		function(){
			swal("Deleted!", "Seu arquivo imaginário foi excluído.", "success");
		});
	});
});